# Building Clean distributions

This document contains notes on how to build distributions of Clean on different platforms.
It describes what we use to build the nightly packages on our jenkins server

## Components of a minimal distribution

A minimal Clean distribution consists of the following components:

- Compiler
- Codegenerator
- Run-time system (distributed as part of StdEnv)
- StdEnv library
- A build tool (clm/cpm/CleanIDE.exe)

# Building on windows-x86

### Tools used to build:

- Msys2 (msys2-i686) with packages: "coreutils" "git", "subversion", "mingw-w64-i686-gcc", "make", "automake", "zip", "unzip"
- Visual Studio Community 2015, with windows SDK's (to build object io)
- Windows SDK for (Windows 7)

### Tools used to run Jenkins:

- Git (32-bit for windows): Necessary for jenkins
- Java 8 Runtime (JRE): Necessary for jenkins

# Building on windows-x64

## Tools used to build:

- Msys2 (msys2-x86\_64) with packages: "coreutils" "git", "subversion", "mingw-w64-x86_64-gcc", "make", "automake", "zip", "unzip"
- Visual Studio Community 2015, with windows SDK's (to build object io)

### Tools used to run Jenkins:

- Git (64-bit for windows): Necessary for jenkins
- Java 8 Runtime (JRE) 64-bit: Necessary for jenkins

Additional notes:

Because of the interaction between the main sh script and an additional windows batch script the PATH environment does not get set correctly. This means that `sed` cannot be found when compiling the runtime system. To work around this, `C:\mys64\usr\bin` needs to be added to windows system-wide path.

# Building on macos-x64

Tools used to run Jenkins:

* Latest macOS (Sierra) with SSH enabled
* Latest Xcode
* Commandline tools for Xcode

# Building on linux-x64

Tools used:

- build-essential (C compilers etc)
- subversion
- git
- curl
