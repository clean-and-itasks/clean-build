#!/usr/bin/env python

# This script analyzes all packages in this repository and generates
# A diagram that shows the relations between the packages and the source
# repositories they use

import os
import cleanbuild

PACKAGE_ROOT = os.path.join(os.path.dirname(__file__), '..')
GRAPHVIZ_EXECUTABLE = "dot"


def writeHtmlFile(packages):
    """Write html output"""
    with open("dependencies.html", "w") as doc:
        doc.write(
            "<!DOCTYPE html>\n"
            "<html>\n<head>\n"
            "<title>Dependencies between Clean packages</title>\n"
            "</head>\n<body>\n"
            "<h1>Dependencies between Clean packages</h1>\n")

        # Intro
        doc.write(
            "<p>\n"
            "Clean has been around for a long time and has a fair amount of legacy code.\n"
            "Historically it has been distributed as a monolithic bundle.\n"
            "The transition to distributing Clean and iTasks as a set of modular packages is a work-in-progress.\n"
            "This overview shows the progress of that process and helps you understand the relations between packages.\n"
            "</p>\n")

        # Include dependency graph
        doc.write(
            "<h2>Dependency graph</h2>\n"
            "<a href=\"dependencies.png\"><img src=\"dependencies.png\" width=\"100%\" ></a>\n"
            "<p>Legend:\n</p>\n<ul>\n"
            "<li>Tabbed rectangles: Deliverable packages (green -> no problems, red -> potential dependency problems)\n"
            "<li>Cylinders: Source code repositories (green -> git, orange -> subversion)\n"
            "<li>Arrow: Dependency\n"
            "</ul>\n")

        # Table of contents
        doc.write("<h2>Available packages:</h2>\n"
                  "<ul>\n")
        for package in packages:
            doc.write("<li><a href=\"#%s\">%s</a></li>\n" %
                      (package["name"], package["name"]))
        doc.write("</ul>\n")

        # Show details for all packages
        for package in packages:
            doc.write("<a name=\"%s\"><h2>%s</h2></a>\n" %
                      (package["name"], package["name"]))
            doc.write("<hr>\n")
            doc.write("<p>Available platforms: %s</p>\n" % (", ".join(
                map(lambda x: x["name"], package["targets"]))))
            # Dependencies
            if len(package["dependencies"]["package"]) > 0:
                doc.write("<p>Depends on packages:</p>\n<ul>\n")

                for dependency in package["dependencies"]["package"]:
                    doc.write("<li><a href=\"#%s\">%s</a></li>" %
                              (dependency, dependency))
                doc.write("</ul>\n")
            # Source repositories
            if len(package["dependencies"]["source"]) > 0:
                doc.write("<p>Built from source repositories:</p>\n<ul>\n")
                for dependency in package["dependencies"]["source"]:
                    doc.write("<li>%s</li>" % dependency["url"])
                doc.write("</ul>\n\n")

            # Issues
            if len(package["issues"]) > 0:
                doc.write(
                    "<p>This package has some issues that need to be addressed</p>\n<ul>\n")
                for issue in package["issues"]:
                    doc.write("<li>%s</li>" % issue)
                doc.write("</ul>\n\n")

            for target in package["targets"]:
                doc.write("<a name=\"%s-%s\"><h3>%s-%s</h3></a>\n" %
                          (package["name"], target["name"],
                           package["name"], target["name"]))
                # Package dependencies
                if len(target["dependencies"]["package"]) > 0:
                    doc.write("<p>Depends on packages:</p>\n<ul>\n")

                    for dependency in target["dependencies"]["package"]:
                        doc.write("<li><a href=\"#%s\">%s</a></li>" %
                                  (dependency, dependency))
                    doc.write("</ul>\n")

                # Source repositories
                if len(target["dependencies"]["source"]) > 0:
                    doc.write("<p>Built from source repositories:</p>\n<ul>\n")
                    for dependency in target["dependencies"]["source"]:
                        doc.write("<li>%s</li>" % dependency["url"])
                    doc.write("</ul>\n\n")
                # Other dependencies
                if len(target["dependencies"]["other"]) > 0:
                    doc.write("<p>Other dependencies:</p>\n<ul>\n")
                    for dependency in target["dependencies"]["other"]:
                        doc.write("<li>%s</li>" % dependency)
                    doc.write("</ul>\n")

        doc.write("</body>\n</html>\n")

# Write graphviz output


def writeDotFile(packages):
    with open("dependencies.dot", "w") as dot:
        dot.write("digraph g {\n")
        dot.write("overlap=false;\n")
        for package in packages:

            if len(package["issues"]) > 0:
                color = "red"
            else:
                color = "green"

            dot.write("\"%s\" [shape=tab,style=filled,fillcolor=%s]\n" % (
                package["name"], color))

            for dependency in package["dependencies"]["source"]:
                # Show repo
                if dependency["type"] == "git":
                    color = "green"
                else:
                    color = "orange"
                dot.write(
                    "\"%s:%s\" [shape=cylinder,style=filled,fillcolor=%s]\n" %
                    (dependency["type"], dependency["name"], color))
                # Link repo to package
                dot.write("\"%s:%s\" -> \"%s\"\n" %
                          (dependency["type"], dependency["name"],
                           package["name"]))

            for dependency in package["dependencies"]["package"]:
                dot.write("\"%s\" -> \"%s\"\n" % (dependency, package["name"]))

        dot.write("}\n")

# Call graphviz program to render the diagram


def generatePngFromDotFile():
    os.system("%s -T png -o dependencies.png dependencies.dot" %
              GRAPHVIZ_EXECUTABLE)


if __name__ == "__main__":
    packages = cleanbuild.indexPackages()
    writeHtmlFile(packages)
    writeDotFile(packages)
    generatePngFromDotFile()
