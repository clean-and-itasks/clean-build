#!/bin/sh
set -e

#to remove when changes are in the master
(
	cd src/abc-interpreter-master;
	git config --global user.email "ci@gitlab.science.ru.nl"
	git config --global user.name "ci"
	git pull origin 103-linux-arm64-interface;
)

cp -r src/abc-interpreter-master build/abc-interpreter

export CLEAN_HOME=`pwd`/build/clean
export PATH="$CLEAN_HOME/bin:$PATH"

# TODO: remove this when gcc>5 is default on the build server.
# This is needed because gcc 5 contains a bug that leads to a segfault for our source.
(cc --version | grep -F 5.) && export CC=gcc-6

# Library
make -C build/abc-interpreter/src optimized arm64 library
(cd build/abc-interpreter/lib;
	curl -L https://gitlab.com/clean-and-itasks/abc-interpreter/-/jobs/artifacts/master/raw/src-js/WebPublic.tar.gz?job=build-wasm \
	| tar -xzv
)

mkdir -p target/clean-abc-interpreter/lib/ABCInterpreter
cp -r build/abc-interpreter/lib/* target/clean-abc-interpreter/lib/ABCInterpreter

rm target/clean-abc-interpreter/lib/ABCInterpreter/Clean\ System\ Files/*_library

# Executables
TOOLS="abcopt bcgen bclink bcprelink bcstrip"

mkdir -p target/clean-abc-interpreter/lib/exe
make -C build/abc-interpreter/src optimized arm64 $TOOLS
for tool in $TOOLS; do
	cp "build/abc-interpreter/src/$tool" target/clean-abc-interpreter/lib/exe
done
