#!/bin/sh
set -e

#Visual studio compiler 
VSSETENV="C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"
COMMAND=`cygpath --unix $COMSPEC`

#Clean tools
export CLEAN_HOME=`cygpath --windows --absolute build/clean`
export PATH="$CLEAN_HOME:$PATH"

mkdir -p build
cp -r src/abc-interpreter-master build/abc-interpreter

# Build
(cd build/abc-interpreter/src
	cat <<EOBATCH | "$COMMAND"
set PATH=$CLEAN_HOME;%PATH%
set CLEAN_HOME=$CLEAN_HOME
@call "$VSSETENV" amd64
@nmake -f Makefile.windows64 all
EOBATCH
)

OS=Windows_NT make -C build/abc-interpreter/src ABCOptimiser.exe

# Library
(cd build/abc-interpreter/lib;
	curl -L https://gitlab.com/clean-and-itasks/abc-interpreter/-/jobs/artifacts/master/raw/src-js/WebPublic.tar.gz?job=build-wasm > webpublic.tgz
	tar -xzvf webpublic.tgz
	rm webpublic.tgz
)
mkdir -p target/clean-abc-interpreter/Libraries/ABCInterpreter
cp -r build/abc-interpreter/lib/* target/clean-abc-interpreter/Libraries/ABCInterpreter

# Executables
mkdir -p target/clean-abc-interpreter/Tools/Clean\ System\ 64
for tool in ABCOptimiser ByteCodeGenerator ByteCodeLinker ByteCodePrelinker ByteCodeStripper; do
	cp "build/abc-interpreter/src/$tool.exe" target/clean-abc-interpreter/Tools/Clean\ System\ 64
done
