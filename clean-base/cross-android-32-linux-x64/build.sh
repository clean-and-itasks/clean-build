#!/bin/sh
set -e
export CUR_CLEAN_HOME=$CLEAN_HOME
export CUR_PATH=$PATH

. ./config.sh # Path to cross compilers
export PATCHES=$(pwd)/patch

#### Component build functions ####

build_codegenerator () { # $1:target 
    mkdir -p build
    cp -r src/CodeGenerator build/codegenerator

    # Patch
    (cd build/codegenerator; sed -i 's/int\ pic_flag=0;/int\ pic_flag=1;/g' cg.c)

    (cd build/codegenerator
	echo "$CFLAGS -m32 -DGNU_C -DLINUX -DLINUX_ELF -DARM -O -fomit-frame-pointer -I."
	make -f "$PATCHES/codegenerator/Makefile.android_arm" cg CC=$CC CFLAGS="$CFLAGS -m32 -DGNU_C -DLINUX -DLINUX_ELF -DARM -O -fomit-frame-pointer -I. -I$PATCHES/codegenerator/includeandroid"
    )

    mkdir -p $1/lib/exe
    cp build/codegenerator/cg $1/lib/exe
}

build_runtimesystem_c () { # $1:target
    mkdir -p build
    cp -r src/RuntimeSystem build/runtimesystem
    (cd build/runtimesystem
	make -f $PATCHES/runtimesystem/Makefile.android_arm CC=$CROSS_CC AS=$CROSS_AS CFLAGS="$CROSS_CFLAGS" LD=$CROSS_LD ASFLAGS="$CROSS_ASFLAGS"
    )
    mkdir -p $1/lib/StdEnv/Clean\ System\ Files

    cp build/runtimesystem/_startup.o $1/lib/StdEnv/Clean\ System\ Files/_startup.o
}
build_runtimesystem_clean () { # $1:target
    #Generate the 'clean part' of the runtime system
    mkdir -p $1/lib/StdEnv/Clean\ System\ Files
    cp src/libraries/StdEnv/Clean\ System\ Files/_system.abc $1/lib/StdEnv/Clean\ System\ Files/_system.abc
    #Use the new code generator
    target/clean-c-components/lib/exe/cg $1/lib/StdEnv/Clean\ System\ Files/_system
}

build_linker () { # $1:target
    mkdir -p build
    cp -r $PATCHES/cclinker build/cclinker
    (cd build/cclinker
	clm -nr -nt -IL Platform -IL Dynamics -IL Generics cclinker -o cclinker
    )
    mkdir -p $1/lib/exe
    cp build/cclinker/cclinker $1/lib/exe/cclinker
    cp build/cclinker/cclinker.config $1/lib/exe/cclinker.config
}

build_cpm () { # $1:target
    #Collect dependencies
    mkdir -p build/cpm/deps/platform
    cp -r src/clean-platform/src/libraries/OS-Independent/* build/cpm/deps/platform/
    cp -r src/clean-platform/src/libraries/OS-Posix/* build/cpm/deps/platform/
    cp -r src/clean-platform/src/libraries/OS-Linux/* build/cpm/deps/platform/
    cp -r src/clean-platform/src/libraries/OS-Linux-64/* build/cpm/deps/platform/

    cp -r src/clean-platform/src/libraries/OS-Independent/Deprecated/StdLib build/cpm/deps/platform-stdlib

    #Get cpm source code
    cp -r src/tools/CleanIDE build/cpm/CleanIDE

    #Patch cpm for ARM
    (cd build/cpm/CleanIDE/Unix; sed -i 's/accFiles (write_options_file/accFiles (write_arm_options_file/g' PmCleanSystem.icl)
    cat $PATCHES/cpm/optionsarm.icl >> build/cpm/CleanIDE/Unix/PmCleanSystem.icl

    (cd build/cpm/CleanIDE/cpm
        clm -h 256M -nr -nt -I Posix -I ../BatchBuild -I ../Pm -I ../Unix -I ../Util -I ../Interfaces/LinkerInterface\
            -I ../../deps/platform -I ../../deps/platform-stdlib -IL ArgEnv -IL Directory -IL Generics Cpm -o cpm
    )

    mkdir -p $1/bin
    cp build/cpm/CleanIDE/cpm/cpm $1/bin/cpm

    mkdir -p $1/etc
    mkdir -p $1/Temp
    cp txt/IDEEnvs $1/etc/IDEEnvs
}

build_compiler() { # $1:target
    mkdir -p build
    cp -r src/compiler build/compiler
    (cd build/compiler
        clm -ABC -nw -ci -I backend -I frontend -I main/Unix -IL ArgEnv backendconvert
        sed s/40M/256M/ < unix/make.linux.sh > unix/make.linux_.sh
        chmod +x unix/make.linux_.sh
        ./unix/make.linux_.sh
    )
    mkdir -p $1/lib/exe
    cp build/compiler/cocl $1/lib/exe/cocl
}

build_stdenv() { # $1:target
    mkdir -p $1/lib/StdEnv
    cp src/libraries/StdEnv/_library.dcl $1/lib/StdEnv/
    cp src/libraries/StdEnv/_startup.dcl $1/lib/StdEnv/
    cp src/libraries/StdEnv/_system.dcl $1/lib/StdEnv/

    for a in StdArray StdBool StdChar StdCharList StdClass StdDebug StdEnum StdEnv \
	  StdFunc StdFunctions StdList StdMisc StdOrdList StdOverloaded StdOverloadedList \
	  StdStrictLists StdTuple _SystemArray _SystemEnum _SystemEnumStrict \
	  _SystemStrictLists StdGeneric;
    do cp src/libraries/StdEnv/$a.[di]cl $1/lib/StdEnv ;
    done

    cp src/libraries/StdEnv/StdInt.icl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdInt.dcl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdFile.dcl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdFile.icl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdReal.dcl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdReal.icl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdString.dcl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdString.icl $1/lib/StdEnv/

    echo first compile of system modules
    for a in StdChar; # compile twice for inlining
    do $1/lib/exe/cocl -P $1/lib/StdEnv $a ;
    done

    echo second compile of system modules
    for a in StdMisc StdBool StdInt StdChar StdFile StdReal StdString;
    do $1/lib/exe/cocl -P $1/lib/StdEnv $a ;
    done
}

build_argenv() { # $1:target
    mkdir -p build/libraries/

    cp -r src/libraries/ArgEnvUnix build/libraries/ArgEnvUnix
    (cd build/libraries/ArgEnvUnix
	make -f Makefile CC=$CC COPTIONS="$CFLAGS"
    )

    cp -r src/libraries/ArgEnvUnix build/libraries/ArgEnvARM
    (cd build/libraries/ArgEnvARM
        make -f Makefile_arm ArgEnvC.o CC=$CROSS_CC COPTIONS="$CROSS_CFLAGS"
    )

    mkdir -p "$1/lib/ArgEnv/Clean System Files"
    for f in ArgEnvC.c ArgEnv.dcl printenv.icl ArgEnv.icl Makefile README;
    do  cp build/libraries/ArgEnvARM/$f $1/lib/ArgEnv/$f
    done
    cp "build/libraries/ArgEnvARM/Clean System Files/ArgEnvC.o" "$1/lib/ArgEnv/Clean System Files/"
}

build_dynamics() { #$1:target
    #Copy minimal Dynamics libraries
    mkdir -p "$1/lib/Dynamics/Clean System Files"
    cp src/libraries/StdDynamicEnv/extension/StdCleanTypes.dcl $1/lib/Dynamics/
    cp src/libraries/StdDynamicEnv/extension/StdCleanTypes.icl $1/lib/Dynamics/
    cp src/libraries/StdDynamicEnv/extension/StdDynamic.dcl $1/lib/Dynamics/
    cp src/libraries/StdDynamicEnv/extension/StdDynamicNoLinker.icl $1/lib/Dynamics/StdDynamic.icl
    cp src/libraries/StdDynamicEnv/implementation/_SystemDynamic.dcl $1/lib/Dynamics/
    cp src/libraries/StdDynamicEnv/implementation/_SystemDynamic.icl $1/lib/Dynamics/
}
build_generics() { #$1:target
    #Copy generics library
    mkdir -p $1/lib/Generics
    cp src/libraries/GenLib/*.[id]cl $1/lib/Generics/
}
build_directory() {
    mkdir -p build/Directory
    cp -r src/libraries/Directory/* build/Directory/
    cp src/tools/htoclean/Clean.h "build/Directory/Clean System Files Unix/Clean.h"

    (cd "build/Directory/Clean System Files Unix"
         $CC $CFLAGS -c -O cDirectory.c
    )

    mkdir -p build/DirectoryARM
    cp -r src/libraries/Directory/* build/DirectoryARM/
    cp src/tools/htoclean/Clean.h "build/DirectoryARM/Clean System Files Unix/Clean.h"

    (cd "build/DirectoryARM/Clean System Files Unix"
         $CROSS_CC $CROSS_CFLAGS -c -O cDirectory.c
    )

    mkdir -p "$1/lib/Directory/Clean System Files"
    cp build/DirectoryARM/*.[id]cl $1/lib/Directory/
    cp build/DirectoryARM/Clean\ System\ Files\ Unix/* $1/lib/Directory/Clean\ System\ Files/
}

build_clean_c_components () { # $1:target
    mkdir -p $1
    build_codegenerator $1
    build_runtimesystem_c $1
}

build_clean() { # $1:target
    mkdir -p $1

    #Copy all C components (they are built only once)
    cp -r target/clean-c-components/* $1/

    #Copy docs
    cp src/tools/CleanIDE/CleanLicenseConditions.txt $1/CleanLicenseConditions.txt
    cp txt/README $1/README.md

    mkdir -p $1/doc
    cp src/language_report/CleanLangRep.2.2.pdf $1/doc/
    mkdir -p $1/doc/CleanLangRep/
    cp src/language_report/*.htm $1/doc/CleanLangRep/
    cp src/language_report/*.css $1/doc/CleanLangRep/
    cp src/language_report/*.png $1/doc/CleanLangRep/
    mkdir -p $1/doc/CleanLangRep/CleanRep.2.2_files/
    cp src/language_report/CleanRep.2.2_files/* $1/doc/CleanLangRep/CleanRep.2.2_files/

    # Build all Clean components
    build_compiler $1
    # Standard libraries
    build_stdenv $1
    # Rts and compiler
    build_runtimesystem_clean $1
    build_linker $1
}

#### Main build process ####

# Build components written in C (code generator, runtime system and clm)
build_clean_c_components target/clean-c-components

# Build a clean system using the boot compiler
export PATH="`pwd`/dependencies/clean/bin":$CUR_PATH
export CLEAN_HOME=`pwd`/dependencies/clean

build_clean target/clean-base
# Add cpm 
build_cpm target/clean-base
