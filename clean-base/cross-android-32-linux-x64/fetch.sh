#!/bin/sh
SVN=svn
SVN_BASEURL="https://svn.cs.ru.nl/repos"
GIT=git
GIT_BASEURL="https://gitlab.com/clean-and-itasks"

REV=""
if [ -n "${CLEANDATE+set}" ]; then
	REV="-r {$CLEANDATE}"
fi

mkdir -p src
$SVN export "$SVN_BASEURL/clean-run-time-system/trunk" "src/RuntimeSystem"
$SVN export "$SVN_BASEURL/clean-code-generator/trunk" "src/CodeGenerator"
$SVN export "$SVN_BASEURL/clean-compiler/branches/itask" "src/compiler"
$SVN export "$SVN_BASEURL/clean-tools/trunk/elf_linker" "src/elf_linker"
$SVN export "$SVN_BASEURL/clean-ide/trunk" "src/tools/CleanIDE"
$SVN export "$SVN_BASEURL/clean-tools/trunk/clm" "src/tools/clm"
$SVN export "$SVN_BASEURL/clean-tools/trunk/htoclean" "src/tools/htoclean"
$SVN export $REV "$SVN_BASEURL/clean-libraries/trunk/Libraries/StdEnv" "src/libraries/StdEnv"
$SVN export "$SVN_BASEURL/clean-libraries/trunk/Libraries/GenLib" "src/libraries/GenLib"
$SVN export "$SVN_BASEURL/clean-libraries/trunk/Libraries/Directory" "src/libraries/Directory"
$SVN export "$SVN_BASEURL/clean-libraries/trunk/Libraries/ArgEnvUnix" "src/libraries/ArgEnvUnix"
$SVN export "$SVN_BASEURL/clean-dynamic-system/trunk/dynamics/StdDynamicEnv" "src/libraries/StdDynamicEnv"
$SVN export "$SVN_BASEURL/clean-language-report/trunk" "src/language_report"
$GIT clone --depth 1 $GIT_BASEURL/clean-platform "src/clean-platform"
