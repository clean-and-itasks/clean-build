set -e

PACKAGE=$1
OS=$2
ARCH=$3

#Where to look for the clean system to build with
export CLEAN_HOME=`pwd`/build/clean
export CLEANPATH=`pwd`/build/clean/lib/StdEnv
export CLEANLIB=`pwd`/build/clean/lib/exe
export PATH=`pwd`/build/clean/bin:$PATH


#### Component build functions ####

build_codegenerator () { # $1:target 
    mkdir -p build
	cp -r src/code-generator-master build/codegenerator
    (cd build/codegenerator
        make -f Makefile.macosx64 cg
    )

	mkdir -p $1/lib/exe
    cp build/codegenerator/cg $1/lib/exe
}

build_runtimesystem_c () { # $1:target
    mkdir -p build
	cp -r src/run-time-system-master build/runtimesystem
	(cd build/runtimesystem/macho64
        source make.sh
    )

    mkdir -p $1/lib/StdEnv/Clean\ System\ Files

    touch $1/lib/StdEnv/_startupProfile.dcl
    touch $1/lib/StdEnv/_startupProfileGraph.dcl
    touch $1/lib/StdEnv/_startupTrace.dcl

    cp build/runtimesystem/macho64/_startup.o $1/lib/StdEnv/Clean\ System\ Files/_startup.o
    cp build/runtimesystem/macho64/_startupProfile.o $1/lib/StdEnv/Clean\ System\ Files/_startupProfile.o
    cp build/runtimesystem/macho64/_startupProfileGraph.o $1/lib/StdEnv/Clean\ System\ Files/_startupProfileGraph.o
    cp build/runtimesystem/macho64/_startupTrace.o $1/lib/StdEnv/Clean\ System\ Files/_startupTrace.o
}
build_runtimesystem_clean () { # $1:target
    #Generate the 'clean part' of the runtime system
    mkdir -p $1/lib/StdEnv/Clean\ System\ Files
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/_system.abc $1/lib/StdEnv/Clean\ System\ Files/_system.abc
    $1/lib/exe/cg $1/lib/StdEnv/Clean\ System\ Files/_system
}

build_clm () { # $1:target
    mkdir -p build
	cp -r src/clm-master build/clm
	(cd build/clm
        make -f Makefile.macho64 clm
	)
	mkdir -p $1/bin
    cp build/clm/clm $1/bin/clm
}

build_batchbuild() { #$1:target

    mkdir -p build/batchbuild/deps/
    cp -r src/stdenv-master/StdMaybe.* build/batchbuild/deps/
    cp -r src/clean-libraries-master/Libraries/StdLib/* build/batchbuild/deps/

    cp -r src/clean-ide-master build/batchbuild/CleanIDE
    (cd build/batchbuild/CleanIDE/BatchBuild
        clm -h 256M -nr -nt -I Posix -I ../Pm -I ../MacOSX\
			-I ../Util -I ../Interfaces/LinkerInterface -I ../../deps/\
			-IL ArgEnv -IL Directory -IL Generics BatchBuild -o batchbuild
	)
	mkdir -p $1/bin
    cp build/batchbuild/CleanIDE/BatchBuild/batchbuild $1/bin/batchbuild

	mkdir -p $1/etc
	mkdir -p $1/Temp
    cp $PACKAGE/$OS-$ARCH/txt/IDEEnvs $1/etc/IDEEnvs
}
build_cpm () { # $1:target
	mkdir -p build/cpm

    #Get cpm source code
    cp -r src/clean-ide-master build/cpm/CleanIDE

	#Add dependencies
    cp -r src/clean-libraries-master/Libraries/StdLib/* build/cpm/CleanIDE/Util/

    (cd build/cpm/CleanIDE
		batchbuild CpmMacOSX.prj
	)
    cp build/cpm/CleanIDE/cpm/cpm $1/bin/cpm
}

build_compiler() { # $1:target
    mkdir -p build
    cp -r src/compiler-itask build/compiler
    (cd build/compiler
	clm -aC,-s,2m -fusion -ABC -nw -ci -I backend -I frontend -I main -I main/Unix -IL ArgEnv backendconvert

        source unix/make.macosx64.sh
    )
    mkdir -p $1/lib/exe
    cp build/compiler/cocl $1/lib/exe/cocl-itasks
}

build_compiler_master() { # $1:target
    mkdir -p build
    cp -r src/compiler-master build/compiler-master
    (cd build/compiler-master
        clm -aC,-s,2m -fusion -ABC -nw -ci -I backend -I frontend -I main -I main/Unix -IL ArgEnv backendconvert

        source unix/make.macosx64.sh
    )
    mkdir -p $1/lib/exe
    cp build/compiler-master/cocl $1/lib/exe/cocl
}

build_docs () { #$1:target
    cp src/clean-ide-master/CleanLicenseConditions.txt $1/CleanLicenseConditions.txt
    cp $PACKAGE/$OS-$ARCH/txt/README $1/README.md
}

build_stdenv() { # $1:target
    mkdir -p $1/lib/StdEnv
    cp src/stdenv-master/_library.dcl $1/lib/StdEnv/
    cp src/stdenv-master/_startup.dcl $1/lib/StdEnv/
    cp src/stdenv-master/_system.dcl $1/lib/StdEnv/

    for a in StdArray StdBool StdChar StdCharList StdClass StdDebug StdEnum StdEnv \
	  StdFunc StdFunctions StdList StdMisc StdOrdList StdOverloaded StdOverloadedList \
	  StdStrictLists StdTuple StdMaybe _SystemArray _SystemEnum _SystemEnumStrict \
	  _SystemStrictLists _SystemStrictMaybes StdGeneric _SystemDynamic StdDynamic;
    do cp src/stdenv-master/$a.[di]cl $1/lib/StdEnv ;
    done

    cp src/stdenv-master/StdInt.icl $1/lib/StdEnv/
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/_SystemStrictLists.icl $1/lib/StdEnv/_SystemStrictLists.icl
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/_SystemStrictMaybes.icl $1/lib/StdEnv/_SystemStrictMaybes.icl
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdInt.dcl $1/lib/StdEnv/
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdFile.dcl $1/lib/StdEnv/
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdFile.icl $1/lib/StdEnv/
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdReal.dcl $1/lib/StdEnv/
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdReal.icl $1/lib/StdEnv/
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdString.dcl $1/lib/StdEnv/
    cp src/stdenv-master/StdEnv\ 64\ Changed\ Files/StdString.icl $1/lib/StdEnv/

    if [ ! -z ${BUILD_STDENV+x} ]; then
        echo first compile of system modules
        for a in StdChar; # compile twice for inlining
        do $CLEAN_HOME/lib/exe/cocl -ou -pm -pt -P $1/lib/StdEnv $a ;
        done

        echo second compile of system modules
        for a in StdMisc StdBool StdInt StdChar StdFile StdReal StdString;
        do $CLEAN_HOME/lib/exe/cocl -ou -pm -pt -P $1/lib/StdEnv $a ;
        done
    fi
}

build_argenv() { # $1:target
    mkdir -p build/libraries/
    cp -r src/clean-libraries-master/Libraries/ArgEnvUnix build/libraries/ArgEnv
    (cd build/libraries/ArgEnv
        make -e
    )

    mkdir -p "$1/lib/ArgEnv/Clean System Files"
    for f in ArgEnvC.c ArgEnv.dcl printenv.icl ArgEnv.icl Makefile README;
    do  cp build/libraries/ArgEnv/$f $1/lib/ArgEnv/$f
    done
    cp "build/libraries/ArgEnv/Clean System Files/ArgEnvC.o" "$1/lib/ArgEnv/Clean System Files/"
}

build_dynamics() { #$1:target
	#Copy minimal Dynamics libraries
    mkdir -p "$1/lib/Dynamics/Clean System Files"
    cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdCleanTypes.dcl $1/lib/Dynamics/
    cp src/clean-dynamic-system-trunk/dynamics/StdDynamicEnv/extension/StdCleanTypes.icl $1/lib/Dynamics/
}

build_generics() { #$1:target
	#Copy generics library
    mkdir -p $1/lib/Generics
    cp src/clean-libraries-master/Libraries/GenLib/*.[id]cl $1/lib/Generics/
}

build_directory() {
	mkdir -p build/Directory
    cp -r src/clean-libraries-master/Libraries/Directory/* build/Directory/
    cp src/clean-tools-trunk/htoclean/Clean.h "build/Directory/Clean System Files Unix/Clean.h"

    (cd "build/Directory/Clean System Files Unix"
         gcc -c -O cDirectory.c
    )
    mkdir -p "$1/lib/Directory/Clean System Files"
    cp build/Directory/*.[id]cl $1/lib/Directory/
    cp build/Directory/Clean\ System\ Files\ Unix/* $1/lib/Directory/Clean\ System\ Files/
}

build_clean_c_components () { # $1:target
    mkdir -p $1
    build_codegenerator $1
    build_runtimesystem_c $1
    build_clm $1
}

build_clean() { # $1:target
    mkdir -p $1

	#Copy all C components (they are built only once)
	cp -r target/clean-c-components/* $1/

    # Build all Clean components
    # Standard libraries
    build_stdenv $1
    # Rts and compiler
    build_runtimesystem_clean $1
    build_compiler $1
    build_compiler_master $1
    # Build tools
	build_batchbuild $1
}

#### Main build process ####

# Build components written in C (code generator, runtime system and clm)
build_clean_c_components target/clean-c-components

# First pass: build a clean system using the boot compiler
build_clean target/clean-intermediate

# Standard libraries as dependencies for the second pass
build_dynamics target/clean-intermediate
build_generics target/clean-intermediate
build_directory target/clean-intermediate
build_argenv target/clean-intermediate

# Second pass: Build a minimal clean system using the clean system from the previous pass
rm -rf build
mkdir -p build/clean
cp -r target/clean-intermediate/* build/clean
export BUILD_STDENV=yes

# TODO: Temporary: the new compiler generates new ABC code (with optimized
# instance calls), so we need to regenerate the ABC of StdEnv before trying to
# build a new Clean system. This needs to be fixed elsewhere.
build_stdenv target/new-stdenv
mv target/new-stdenv/lib/StdEnv/Clean\ System\ Files/*.abc build/clean/lib/StdEnv/Clean\ System\ Files
rm -rf target/new-stdenv

build_clean target/clean-base
build_cpm target/clean-base
build_docs target/clean-base

