set -e
./clean-classic/linux-arm32/get_sources.sh
PWD=`pwd`
export OLDPATH=$PATH
export BOOTCOMPILERPATH=$PWD/boot_compiler/bin:$PWD/boot_compiler/lib/exe:$PATH
./clean-classic/linux-arm32/build_cg.sh
./clean-classic/linux-arm32/build_clm.sh
./clean-classic/linux-arm32/build_rts.sh
./clean-classic/linux-arm32/build_boot_compiler.sh
export PATH=$BOOTCOMPILERPATH
./clean-classic/linux-arm32/build_htoclean.sh
./clean-classic/linux-arm32/build_clc.sh
./clean-classic/linux-arm32/build_batch_build.sh
./clean-classic/linux-arm32/build_libraries.sh
./clean-classic/linux-arm32/build_language_report.sh
./clean-classic/linux-arm32/build_clean.sh
./clean-classic/linux-arm32/build_stdenv.sh
./clean-classic/linux-arm32/build_examples.sh
export PATH=$OLDPATH
mv clean clean0
(cd clean0 ; make)
export PATH=$PWD/clean0:$PWD/clean0/bin:$PWD/clean0/lib/exe:$OLDPATH
mkdir step0
mv stdenv step0/
mv clean-libraries step0/
mv compiler step0/
mv write_clean_manual step0/
mv htoclean step0/
mv clean-ide step0/
mv clean-platform step0/
./clean-classic/linux-arm32/build_htoclean.sh
./clean-classic/linux-arm32/build_clc.sh
./clean-classic/linux-arm32/build_batch_build.sh
./clean-classic/linux-arm32/build_libraries.sh
./clean-classic/linux-arm32/build_language_report.sh
./clean-classic/linux-arm32/build_clean.sh
./clean-classic/linux-arm32/build_stdenv.sh
./clean-classic/linux-arm32/build_examples.sh

