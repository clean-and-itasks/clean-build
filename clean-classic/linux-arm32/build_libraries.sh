set -e
(cd git/clean-libraries; git checkout master \
 Libraries/Directory Libraries/TCPIP Libraries/StdLib Libraries/GenLib Libraries/MersenneTwister)

mkdir -p clean-libraries/Libraries
mv git/clean-libraries/Libraries/* clean-libraries/Libraries/

(cd git/gast; git checkout master LICENSE Libraries/Gast.dcl Libraries/Gast.icl Libraries/Gast)
mkdir -p clean-libraries/Libraries
mv git/gast/Libraries clean-libraries/Libraries/Gast/
mv git/gast/LICENSE clean-libraries/Libraries/Gast/LICENSE

(cd git/clean-platform; git checkout master .)
mkdir clean-platform
mv git/clean-platform/* clean-platform/
mv clean-platform/LICENSE clean-platform/LICENSE.Clean
sed -e '1,/License/d' -e 's/LICENSE].LICENCE/LICENCE.Clean](LICENSE.Clean/' -e 's/$/\r/' < clean-platform/README.md > clean-platform/LICENSE

cp htoclean/Clean.h "clean-libraries/Libraries/Directory/Clean System Files Unix/Clean.h"
(cd "clean-libraries/Libraries/Directory/Clean System Files Unix"; gcc -c -O cDirectory.c)

cp htoclean/Clean.h "clean-libraries/Libraries/TCPIP/Linux_C/Clean.h"
(cd "clean-libraries/Libraries/TCPIP/Linux_C"; gcc -c -O cTCP_121.c)
(cd "clean-libraries/Libraries/TCPIP"; sed 's/, library "wsock_library"//' <ostcp.icl >ostcp.icl_; rm -f ostcp.icl; mv ostcp.icl_ ostcp.icl)
(cd "clean-libraries/Libraries/TCPIP/Linux_C"; sed 's/, library "wsock_library"//' <ostcp.icl >ostcp.icl_; rm -f ostcp.icl; mv ostcp.icl_ ostcp.icl)

(cd clean-platform/src/cdeps; make)

