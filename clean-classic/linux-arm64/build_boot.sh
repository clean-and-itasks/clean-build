set -e
mkdir -p boot
cp -r -p clean boot/clean
(cd boot/clean; make)
mv boot/clean boot/clean0
cp -r -p clean boot/clean
cp ./clean-classic/linux-arm64/txt/README.boot boot/clean/README
./clean-classic/linux-arm64/remove_bin.sh
for m in StdArray StdEnv StdOverloaded _SystemArray StdOverloadedList \
	_SystemEnum StdFunc StdFunctions _SystemEnumStrict StdCharList StdStrictLists \
	_SystemStrictLists StdClass StdList StdDebug StdTuple StdEnum \
	StdOrdList StdGeneric StdMaybe;
do cp -p "boot/clean0/lib/StdEnv/Clean System Files/"$m.abc "boot/clean/StdEnv/Clean System Files/"$m.abc;
done

mkdir -p boot/clean/src
cp ./clean-classic/linux-arm64/txt/Makefile_boot boot/clean/src/Makefile
cd boot/clean/src
../../../clean-classic/linux-arm64/git_clone.sh clean-compiler-and-rts/run-time-system.git RuntimeSystem
rm -Rf RuntimeSystem/.git
../../../clean-classic/linux-arm64/git_clone.sh clean-compiler-and-rts/code-generator.git CodeGenerator
rm -Rf CodeGenerator/.git
../../../clean-classic/linux-arm64/git_clone.sh clean-and-itasks/clm.git clm
rm -Rf clm/.git
../../../clean-classic/linux-arm64/svn_export.sh clean-tools/trunk/htoclean tools/htoclean
../../../clean-classic/linux-arm64/git_clone_no_checkout_depth_1.sh clean-and-itasks/clean-libraries.git clean-libraries
(cd clean-libraries; git checkout master Libraries/ArgEnvUnix Libraries/StdLib Libraries/Directory)
rm -Rf clean-libraries/.git
(cp "tools/htoclean/Clean.h" "clean-libraries/Libraries/Directory/Clean System Files Unix/Clean.h"; cd clean-libraries/Libraries/Directory; mkdir "Clean System Files"; gcc -c -O "Clean System Files Unix/cDirectory.c" -o "Clean System Files/cDirectory.o")
../../../clean-classic/linux-arm64/git_clone.sh clean-compiler-and-rts/compiler.git compiler
rm -Rf compiler/.git
../../../clean-classic/linux-arm64/git_clone.sh clean-and-itasks/clean-ide.git clean-ide
rm -Rf clean-ide/.git
chmod +x RuntimeSystem/remove_tmp_files_linux
cd ../../..
for p in compiler/main/Unix \
	clean-libraries/Libraries/ArgEnvUnix compiler/frontend compiler/backend compiler/main;
do  mkdir -p "boot/clean/src/$p/Clean System Files";
    cp "$p/Clean System Files/"*.abc "boot/clean/src/$p/Clean System Files";
done
