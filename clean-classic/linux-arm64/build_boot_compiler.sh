mkdir -p boot_compiler/lib/exe
cp code-generator/cg boot_compiler/lib/exe/cg
cp run-time-system/_startup.o boot_compiler/StdEnv/Clean\ System\ Files/_startup.o
cp clm/clm boot_compiler/bin/clm
cp clm/patch_bin boot_compiler/bin/patch_bin
cd boot_compiler
bin/patch_bin bin/clm CLEANPATH `pwd`/lib/StdEnv
bin/patch_bin bin/clm CLEANLIB `pwd`/lib/exe
bin/patch_bin bin/clm CLEANILIB `pwd`/lib
mv StdEnv lib/StdEnv
sed -i 's/dINT/INT/g' lib/StdEnv/Clean\ System\ Files/_system.abc
lib/exe/cg lib/StdEnv/Clean\ System\ Files/_system
cp -R data/ArgEnv lib/
mkdir -p lib/ArgEnv/Clean\ System\ Files
(cd lib/ArgEnv; gcc -O -c ArgEnvC.c -o Clean\ System\ Files/ArgEnvC.o)
cp -R data/StdLib lib/
cp -R data/Directory lib/
(cd lib/Directory/Clean\ System\ Files; gcc -O -c cDirectory.c)
(cd src/compiler; export PATH=../../bin:$PATH; unix/make.linux64.sh )
cp src/compiler/cocl lib/exe
cd ..
