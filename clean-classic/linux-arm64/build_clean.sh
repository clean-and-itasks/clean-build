mkdir -p clean
cat <<EOF | cat - clean-ide/CleanLicenseConditions.txt > clean/CleanLicenseConditions.txt
Clean is distributed under the terms of the simplified BSD license below, except for
the Platform and Gast libraries and examples.
The files and BSD licenses of these libraries and examples can be found in the directories
data/Platform, data/Gast, examples/PlatformExamples and examples/GastExamples,
after installation also in lib/Platform and lib/Gast.

EOF
cp clean-classic/linux-arm64/txt/Makefile clean
cp clean-classic/linux-arm64/txt/README clean

mkdir -p clean/bin
cp clm/clm clean/bin
cp clm/patch_bin clean/bin
cp htoclean/htoclean\ source\ code/htoclean clean/bin
cp clean-ide/BatchBuild/BatchBuild clean/bin
cp clean-ide/cpm/cpm clean/bin

mkdir -p clean/exe
cp compiler/cocl clean/exe
cp compiler_itask/cocl clean/exe/cocl_itask
cp code-generator/cg clean/exe

mkdir -p clean/doc
cp write_clean_manual/CleanLanguageReport.pdf clean/doc
mkdir -p clean/doc/CleanLanguageReportHtml
cp write_clean_manual/CleanLanguageReport?*.html clean/doc/CleanLanguageReportHtml
( cd clean/doc/CleanLanguageReportHtml; mv CleanLanguageReportT.html CleanLanguageReport.html )
cp htoclean/Clean.h clean/doc
cp htoclean/CallingCFromClean.html clean/doc
mkdir -p clean/doc/Examples
cp htoclean/Examples/*.[ch] clean/doc/Examples
cp htoclean/Examples/*.[id]cl clean/doc/Examples
cp htoclean/Examples/*.prj clean/doc/Examples
cp htoclean/Examples/*.bat clean/doc/Examples

mkdir -p clean/man/man1
cp clm/clm.1 clean/man/man1

mkdir -p clean/data/ArgEnv
for f in ArgEnvC.c ArgEnv.dcl printenv.icl ArgEnv.icl Makefile README ; do
	cp "clean-libraries/Libraries/ArgEnvUnix/$f" "clean/data/ArgEnv/$f"
done

mkdir -p clean/data/Generics
cp clean-libraries/Libraries/GenLib/*.[id]cl clean/data/Generics/

mkdir -p clean/data/Gast
cp -r -t clean/data/Gast clean-libraries/Libraries/Gast/*

mkdir -p clean/data/StdLib
cp clean-libraries/Libraries/StdLib/*.[id]cl clean/data/StdLib/

mkdir -p "clean/data/Directory/Clean System Files"
cp clean-libraries/Libraries/Directory/* clean/data/Directory
cp "clean-libraries/Libraries/Directory/Clean System Files Unix"/* "clean/data/Directory/Clean System Files"

mkdir -p "clean/data/MersenneTwister/Clean System Files"
cp clean-libraries/Libraries/MersenneTwister/*.* clean/data/MersenneTwister

mkdir -p "clean/data/TCPIP/Clean System Files"
cp clean-libraries/Libraries/TCPIP/TCPIP.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPIP.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPDef.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPDef.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPEvent.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPEvent.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPChannelClass.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPChannelClass.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPChannels.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPChannels.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPStringChannels.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPStringChannels.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPStringChannelsInternal.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/TCPStringChannelsInternal.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/tcp_bytestreams.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/tcp_bytestreams.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/Linux_C/tcp.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/Linux_C/tcp.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/Linux_C/ostcp.dcl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/Linux_C/ostcp.icl clean/data/TCPIP
cp clean-libraries/Libraries/TCPIP/Linux_C/cTCP_121.o "clean/data/TCPIP/Clean System Files"
cp clean-libraries/Libraries/TCPIP/Linux_C/cTCP_121.c "clean/data/TCPIP/Clean System Files"
cp clean-libraries/Libraries/TCPIP/Linux_C/cTCP_121.h "clean/data/TCPIP/Clean System Files"

mkdir -p clean/data/Platform
cp clean-platform/LICENSE clean/data/Platform/LICENSE
cp clean-platform/LICENSE.Clean clean/data/Platform/LICENSE.Clean
cp clean-platform/LICENSE.BSD3 clean/data/Platform/LICENSE.BSD3
cp -r -t clean/data/Platform clean-platform/src/libraries/OS-Independent/*
cp -r -t clean/data/Platform clean-platform/src/libraries/OS-Posix/*
cp -r -t clean/data/Platform clean-platform/src/libraries/OS-Linux/*
cp -r -t clean/data/Platform clean-platform/src/libraries/OS-Linux-64/*
cp -r -t clean/data/Platform clean-platform/src/libraries/Platform-ARM/*
mkdir -p "clean/data/Platform/Clean System Files"
cp clean-platform/src/cdeps/bsearch.o "clean/data/Platform/Clean System Files/"
cp clean-platform/src/cdeps/systemsignal.o "clean/data/Platform/Clean System Files/"
cp clean-platform/src/cdeps/systemprocess.o "clean/data/Platform/Clean System Files/"
cp clean-platform/src/cdeps/WCsubst.o "clean/data/Platform/Clean System Files/"

