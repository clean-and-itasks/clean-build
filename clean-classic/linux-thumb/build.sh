set -e
./clean-classic/linux-thumb/get_sources.sh
PWD=`pwd`
export OLDPATH=$PATH
export BOOTCOMPILERPATH=$PWD/boot_compiler/bin:$PWD/boot_compiler/lib/exe:$PATH
./clean-classic/linux-thumb/build_cg.sh
./clean-classic/linux-thumb/build_clm.sh
./clean-classic/linux-thumb/build_rts.sh
./clean-classic/linux-thumb/build_boot_compiler.sh
export PATH=$BOOTCOMPILERPATH
./clean-classic/linux-thumb/build_htoclean.sh
./clean-classic/linux-thumb/build_clc.sh
./clean-classic/linux-thumb/build_batch_build.sh
./clean-classic/linux-thumb/build_libraries.sh
./clean-classic/linux-thumb/build_language_report.sh
./clean-classic/linux-thumb/build_clean.sh
./clean-classic/linux-thumb/build_stdenv.sh
./clean-classic/linux-thumb/build_examples.sh
export PATH=$OLDPATH
mv clean clean0
(cd clean0 ; make)
export PATH=$PWD/clean0:$PWD/clean0/bin:$PWD/clean0/lib/exe:$OLDPATH
mkdir step0
mv stdenv step0/
mv clean-libraries step0/
mv compiler step0/
mv write_clean_manual step0/
mv htoclean step0/
mv clean-ide step0/
mv clean-platform step0/
./clean-classic/linux-thumb/build_htoclean.sh
./clean-classic/linux-thumb/build_clc.sh
./clean-classic/linux-thumb/build_batch_build.sh
./clean-classic/linux-thumb/build_libraries.sh
./clean-classic/linux-thumb/build_language_report.sh
./clean-classic/linux-thumb/build_clean.sh
./clean-classic/linux-thumb/build_stdenv.sh
./clean-classic/linux-thumb/build_examples.sh

