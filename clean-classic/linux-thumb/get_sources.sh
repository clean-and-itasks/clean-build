mkdir git
cd git
git clone -n --depth=1 https://gitlab.science.ru.nl/clean-compiler-and-rts/code-generator.git
git clone -n --depth=1 https://gitlab.science.ru.nl/clean-compiler-and-rts/run-time-system.git
git clone -n --depth=1 https://gitlab.science.ru.nl/clean-compiler-and-rts/compiler.git
git clone -n --depth=1 https://gitlab.science.ru.nl/clean-compiler-and-rts/language-report.git
git clone -n --depth=1 https://gitlab.science.ru.nl/clean-compiler-and-rts/stdenv.git
git clone -n --depth=1 https://gitlab.science.ru.nl/clean-and-itasks/clm.git
git clone -n --depth=1 https://gitlab.science.ru.nl/clean-and-itasks/clean-ide.git
git clone -n --depth=1 https://gitlab.science.ru.nl/clean-and-itasks/clean-libraries.git
git clone -n --depth=1 https://www.gitlab.com/clean-and-itasks/gast.git gast
git clone -n --depth=1 https://www.gitlab.com/clean-and-itasks/clean-platform.git
cd ..
mkdir svn
cd svn
svn export https://svn.cs.ru.nl/repos/clean-tools/trunk clean-tools
svn export https://svn.cs.ru.nl/repos/clean-dynamic-system/trunk/dynamics/Examples/Examples\ No\ Linker DynamicsExamples
cd ..
