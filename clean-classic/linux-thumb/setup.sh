rm -rf Documentation Tools Fonts README.md
rm -rf clean-abc-interpreter  clean-lib-gast       clean-lib-platform
rm -rf clean-base             clean-language-report  clean-lib-generics   clean-lib-stdlib
rm -rf clean-bundle-complete  clean-lib-argenv       clean-lib-graphcopy  clean-lib-tcpip
rm -rf clean-bundle-itasks    clean-lib-directory    clean-lib-itasks     clean-test
rm -rf clean-lib-dynamics     clean-lib-objectio     clean-ide            clean-test-properties
rm -rf clean-classic/linux-arm32
rm -rf clean-classic/linux-arm64
# rm -rf clean-classic/linux-thumb
rm -rf clean-classic/linux-x64
rm -rf clean-classic/linux-x86
rm -rf clean-classic/macos-x64
rm -rf clean-classic/windows-x64
rm -rf clean-classic/windows-x86

curl  --cacert geant.pem -L -o boot_clean.tgz https://ftp.cs.ru.nl/Clean/Clean30/linux/clean3.0_32_boot.tar.gz

rm -Rf boot_compiler
tar -xzf boot_clean.tgz
mv clean boot_compiler

