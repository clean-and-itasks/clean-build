set -e
mkdir -p boot
cp -r -p clean boot/clean
(cd boot/clean; make)
mv boot/clean boot/clean0
cp -r -p clean boot/clean
cp ./clean-classic/linux-x64/txt/README.boot boot/clean/README
./clean-classic/linux-x64/remove_bin.sh
for m in StdArray StdEnv StdOverloaded _SystemArray StdOverloadedList \
	_SystemEnum StdFunc StdFunctions _SystemEnumStrict StdCharList StdStrictLists \
	_SystemStrictLists StdClass StdList StdDebug StdTuple StdEnum \
	StdOrdList StdGeneric StdMaybe;
do cp -p "boot/clean0/lib/StdEnv/Clean System Files/"$m.abc "boot/clean/StdEnv/Clean System Files/"$m.abc;
done

mkdir -p boot/clean/src
cp ./clean-classic/linux-x64/txt/Makefile_boot boot/clean/src/Makefile
cd boot/clean/src
#../../../clean-classic/linux-x64/git_clone.sh clean-compiler-and-rts/run-time-system.git RuntimeSystem
#rm -Rf RuntimeSystem/.git
(cd ../../../git/run-time-system; git checkout master .)
mkdir RuntimeSystem
mv ../../../git/run-time-system/* RuntimeSystem
#../../../clean-classic/linux-x64/git_clone.sh clean-compiler-and-rts/code-generator.git CodeGenerator
#rm -Rf CodeGenerator/.git
(cd ../../../git/code-generator; git checkout master .)
mkdir CodeGenerator
mv ../../../git/code-generator/* CodeGenerator/
#../../../clean-classic/linux-x64/git_clone.sh clean-and-itasks/clm.git clm
#rm -Rf clm/.git
(cd ../../../git/clm; git checkout master .)
mkdir clm
mv ../../../git/clm/* clm/
#../../../clean-classic/linux-x64/svn_export.sh clean-tools/trunk/htoclean tools/htoclean
mkdir -p tools/htoclean
cp -p -R ../../../svn/clean-tools/htoclean/* tools/htoclean
#../../../clean-classic/linux-x64/git_clone_no_checkout_depth_1.sh clean-and-itasks/clean-libraries.git clean-libraries
#(cd clean-libraries; git checkout master Libraries/ArgEnvUnix Libraries/StdLib Libraries/Directory)
#rm -Rf clean-libraries/.git
(cd ../../../git/clean-libraries; git checkout master Libraries/ArgEnvUnix Libraries/StdLib Libraries/Directory)
mkdir clean-libraries
mv ../../../git/clean-libraries/* clean-libraries/
(cp "tools/htoclean/Clean.h" "clean-libraries/Libraries/Directory/Clean System Files Unix/Clean.h"; cd clean-libraries/Libraries/Directory; mkdir "Clean System Files"; gcc -c -O "Clean System Files Unix/cDirectory.c" -o "Clean System Files/cDirectory.o")
#../../../clean-classic/linux-x64/git_clone.sh clean-compiler-and-rts/compiler.git compiler
#rm -Rf compiler/.git
(cd ../../../git/compiler; git checkout master .)
mkdir compiler
mv ../../../git/compiler/* compiler/
(cd ../../../git/compiler; git checkout origin/itask -- .)
mkdir compiler_itask
mv ../../../git/compiler/* compiler_itask/
#../../../clean-classic/linux-x64/svn_export.sh clean-tools/trunk/elf_linker elf_linker
mkdir -p elf_linker
cp -p -R ../../../svn/clean-tools/elf_linker/* elf_linker
#../../../clean-classic/linux-x64/git_clone.sh clean-and-itasks/clean-ide.git clean-ide
#rm -Rf clean-ide/.git
(cd ../../../git/clean-ide; git checkout master .)
mkdir clean-ide
mv ../../../git/clean-ide/* clean-ide/
chmod +x RuntimeSystem/remove_tmp_files_linux
cd ../../..
sleep 2
for p in elf_linker elf_linker/ai64 compiler/main/Unix compiler/frontend compiler/backend compiler/main;
do  mkdir -p "boot/clean/src/$p/Clean System Files";
    cp "$p/Clean System Files/"*.abc "boot/clean/src/$p/Clean System Files";
done
(cd boot/clean0; make)
./boot/clean0/bin/clm -I ./boot/clean/src/clean-libraries/Libraries/ArgEnvUnix -ABC ArgEnv
