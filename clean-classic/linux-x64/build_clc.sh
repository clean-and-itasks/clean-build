set -e
# ./git_clone.sh clean-compiler-and-rts/compiler.git compiler
(cd git/compiler; git checkout master .)
mkdir compiler
mv git/compiler/* compiler/

cd compiler
clm -ABC -nw -ci -fusion -I backend -I frontend -I main -I main/Unix -IL ArgEnv backendconvert
unix/make.linux64.sh
cd ..

