set -e
(cd git/compiler; git checkout origin/itask -- .)
mkdir compiler_itask
mv git/compiler/* compiler_itask/

cd compiler_itask
clm -ABC -nw -ci -I backend -I frontend -I main -I main/Unix -IL ArgEnv backendconvert
unix/make.linux64.sh
cd ..
