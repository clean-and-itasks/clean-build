(cd git/clean-libraries; git checkout master Examples/Small\ Examples Examples/GenLib)
mkdir -p clean-libraries/Examples
mv git/clean-libraries/Examples/Small\ Examples clean-libraries/Examples
mv git/clean-libraries/Examples/GenLib clean-libraries/Examples

mkdir -p clean/examples/SmallExamples
for a in acker copyfile e fsieve hamming invperm lqueen mulmat nfib pascal reverse revtwice \
	rfib sieve squeen str_arit stwice tak twice war_seq;
do cp clean-libraries/Examples/Small\ Examples/$a.icl clean/examples/SmallExamples
done
cp clean-libraries/Examples/Small\ Examples/run_all_programs clean/examples/SmallExamples
cp clean-libraries/Examples/Small\ Examples/Makefile clean/examples/SmallExamples/Makefile

mkdir -p clean/examples/GenericsExamples
cp clean-libraries/Examples/GenLib/*.icl clean/examples/GenericsExamples

cat > clean/examples/GenericsExamples/make.sh<< EOF
export PATH=../../bin:\$PATH
clm -nt eq -o eq
clm -nt freevars -o freevars
clm -nt fromStr -o fromStr
clm -nt mapSt -o mapSt
clm -nt reduce -o reduce
clm -nt -IL Generics toStr -o toStr
clm -nt value -o value
clm -nt zip -o zip
EOF
chmod +x clean/examples/GenericsExamples/make.sh

cp -r svn/DynamicsExamples clean/examples/

cat > clean/examples/DynamicsExamples/make.sh<< EOF
export PATH=../../bin:\$PATH
clm -nt -dynamics marco -o marco
clm -nt -dynamics t -o t
clm -nt -dynamics type_dependent_functions -o type_dependent_functions
EOF
chmod +x clean/examples/DynamicsExamples/make.sh

(cd git/gast; git checkout master LICENSE Tests)
mkdir -p clean/examples/GastExamples
mv git/gast/LICENSE clean/examples/GastExamples/LICENSE
mv git/gast/Tests/*.icl clean/examples/GastExamples
rm -rf git/gast/Tests

cat > clean/examples/GastExamples/make.sh<< EOF
export PATH=../../bin:\$PATH
clm -nt -b -IL Gast -IL Platform bool -o bool
clm -nt -b -IL Gast -IL Platform char -o char
clm -nt -b -IL Gast -IL Platform int -o int
clm -nt -b -IL Gast -IL Platform tree -o tree
clm -nt -nr -IL Gast -IL Platform with_options -o with_options
EOF
chmod +x clean/examples/GastExamples/make.sh

mkdir -p clean/examples/PlatformExamples/socket
mkdir -p clean/examples/PlatformExamples/WebPM/icons
cp clean-platform/LICENSE clean/examples/PlatformExamples/LICENSE
cp clean-platform/LICENSE.Clean clean/examples/PlatformExamples/LICENSE.Clean
cp clean-platform/LICENSE.BSD3 clean/examples/PlatformExamples/LICENSE.BSD3
cp clean-platform/src/examples/*.* clean/examples/PlatformExamples
cp clean-platform/src/examples/socket/*.icl clean/examples/PlatformExamples/socket
cp clean-platform/src/examples/WebPM/*.* clean/examples/PlatformExamples/WebPM
cp clean-platform/src/examples/WebPM/icons/*.* clean/examples/PlatformExamples/WebPM/icons

cat > clean/examples/PlatformExamples/make.sh<< EOF
export PATH=../../bin:\$PATH
clm -nt -IL Platform IPLookup -o IPLookup
clm -nt -IL Platform ProcessDemo -o ProcessDemo
clm -nt -IL Platform MapDemo -o MapDemo
clm -nt -IL Platform SQLDbDemo -o SQLDbDemo
clm -aC,-h,600m -nt -IL Platform -IL TCPIP WebDemo -o WebDemo
EOF
chmod +x clean/examples/PlatformExamples/make.sh

cat > clean/examples/PlatformExamples/WebPM/make.sh<< EOF
export PATH=../../../bin:\$PATH
clm -aC,-h,600m -nt -IL Platform -IL TCPIP PM -o PM
EOF
chmod +x clean/examples/PlatformExamples/WebPM/make.sh

cat > clean/examples/PlatformExamples/socket/make.sh<< EOF
export PATH=../../../bin:\$PATH
clm -nt -IL Platform client -o client
clm -nt -IL Platform server -o server
EOF
chmod +x clean/examples/PlatformExamples/socket/make.sh
