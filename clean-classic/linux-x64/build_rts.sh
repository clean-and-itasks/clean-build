# ./git_clone.sh clean-compiler-and-rts/run-time-system.git RuntimeSystem
(cd git/run-time-system; git checkout master .)
mkdir run-time-system
mv git/run-time-system/* run-time-system

cd run-time-system
chmod +x ./remove_tmp_files_linux
./remove_tmp_files_linux
./make_astartup.csh
./remove_tmp_files_linux
./make_astartupProfile.csh
./remove_tmp_files_linux
./make_astartupTrace.csh
cd ..
