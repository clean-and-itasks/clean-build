set -e
PWD=`pwd`
export OLDPATH=$PATH
export BOOTCOMPILERPATH=$PWD/boot_compiler/bin:$PWD/boot_compiler/lib/exe:$PATH
./clean-classic/linux-x86/get_sources.sh
./clean-classic/linux-x86/build_cg32.sh
./clean-classic/linux-x86/build_clm32.sh
./clean-classic/linux-x86/build_rts32.sh
export PATH=$BOOTCOMPILERPATH
./clean-classic/linux-x86/build_htoclean.sh
./clean-classic/linux-x86/build_clc32.sh
./clean-classic/linux-x86/build_clc_itasks32.sh
./clean-classic/linux-x86/build_libraries.sh
./clean-classic/linux-x86/build_linker32.sh
./clean-classic/linux-x86/build_batch_build.sh
./clean-classic/linux-x86/build_language_report.sh
./clean-classic/linux-x86/build_clean.sh
./clean-classic/linux-x86/build_stdenv32.sh
./clean-classic/linux-x86/build_examples.sh
export PATH=$OLDPATH
mv clean clean0
(cd clean0 ; make)
export PATH=$PWD/clean0:$PWD/clean0/bin:$PWD/clean0/lib/exe:$OLDPATH
mkdir step0
mv stdenv step0/
mv clean-libraries step0/
mv compiler step0/
mv compiler_itask step0/
mv write_clean_manual step0/
mv htoclean step0/
mv elf_linker step0/
mv clean-ide step0/
mv clean-platform step0/
./clean-classic/linux-x86/build_htoclean.sh
./clean-classic/linux-x86/build_clc32.sh
./clean-classic/linux-x86/build_clc_itasks32.sh
./clean-classic/linux-x86/build_libraries.sh
./clean-classic/linux-x86/build_linker32.sh
./clean-classic/linux-x86/build_batch_build.sh
./clean-classic/linux-x86/build_language_report.sh
./clean-classic/linux-x86/build_clean.sh
./clean-classic/linux-x86/build_stdenv32.sh
./clean-classic/linux-x86/build_examples.sh

mkdir -p target/clean-classic
mv clean/* target/clean-classic/
