set -e
PWD=`pwd`
export OLDPATH=$PATH
./build_cg32.sh
./build_clm32.sh
./build_rts32.sh
export PATH=$PWD/boot_compiler/bin:$PWD/boot_compiler/lib/exe:$OLDPATH
./build_htoclean.sh
./build_clc_itasks32.sh
./build_libraries.sh
./build_linker32.sh
./build_clean.sh
./build_stdenv32.sh
./build_examples.sh
export PATH=$OLDPATH
mv clean clean0
cd clean0
make
cd ..
export PATH=$PWD/clean0:$PWD/clean0/bin:$PWD/clean0/lib/exe:$OLDPATH
mkdir step0
mkdir step0/libraries
mv libraries/StdEnv step0/libraries/StdEnv
mv libraries/ArgEnvUnix step0/libraries/ArgEnvUnix
mv libraries/GenLib step0/libraries/GenLib
mv libraries/StdLib step0/libraries/StdLib
mv compiler step0/compiler
mkdir step0/tools
mv tools/htoclean step0/tools/htoclean
mv tools/elf_linker step0/tools/elf_linker
mkdir step0/tools/CleanIDE
mv tools/CleanIDE/CleanLicenseConditions.txt step0/tools/CleanIDE/CleanLicenseConditions.txt
mkdir step0/tools/CleanIDE/Help
#mv tools/CleanIDE/Help/CleanLangRep.2.1.pdf step0/tools/CleanIDE/Help/CleanLangRep.2.1.pdf
mkdir step0/CleanExamples
mv "CleanExamples/Small Examples" "step0/CleanExamples/Small Examples"
./build_htoclean.sh
./build_clc_itasks32.sh
./build_libraries.sh
./build_linker32.sh
./build_clean_itasks.sh
./build_stdenv32.sh
./build_examples.sh

mkdir clean0/Temp
touch clean0/Temp/errors

# Batchbuild
./build_batchbuild.sh
cp tools/CleanIDE/BatchBuild/batch_build clean0
mv tools/CleanIDE/BatchBuild/batch_build clean
cp IDEEnvs32 clean0/IDEEnvs

# SAPL
./build_sapl32.sh
mv sapldynamics/sapl-collector-linker clean/exe
echo 'cp -R Sapl $INSTALL_STDENV_DIR' >> clean/StdEnv/install.sh
echo 'cp -R Sapl $INSTALL_STDENV_DIR' >> clean0/StdEnv/install.sh

# Building clean-platform
./build_platform.sh
mv platform clean/data/clean-platform

# Building iTasks
./build_itasks32.sh
cp -R iTasks-SDK/Installation/Files/Sapl clean/StdEnv
mv iTasks-SDK clean/data/iTasks-SDK

mkdir clean/etc
cp IDEEnvs32 clean/etc/IDEEnvs
cp IDEEnvs32 clean/IDEEnvs

mkdir clean/Temp
touch clean/Temp/errors

cp -R clean clean1
export PATH=$PWD/clean1:$PWD/clean1/bin:$PWD/clean1/lib/exe:$OLDPATH

cd clean1
make
cd ..

# CPM
./build_cpm.sh
cp tools/CleanIDE/cpm/cpm clean/bin
mv tools/CleanIDE/cpm/cpm clean1/bin

# Remove BatchBuild
rm -f clean/batch_build
rm -f clean/IDEEnvs

export PATH=$PWD/clean/bin:$PWD/clean/lib/exe:$OLDPATH

#./test_itasks.sh
