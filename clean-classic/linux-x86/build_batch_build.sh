set -e
# ./git_clone.sh clean-and-itasks/clean-ide.git clean-ide
(cd git/clean-ide; git checkout master .)
mkdir clean-ide
mv git/clean-ide/* clean-ide/

cd clean-ide
clm -nt -nr -h 20m -I BatchBuild -I Pm -I Unix -I Unix/Intel -I Util -I Interfaces/LinkerInterface -IL StdLib -IL Directory -IL ArgEnv BatchBuild -o BatchBuild/BatchBuild
clm -nt -nr -h 20m -I cpm -I cpm/Posix -I BatchBuild -I Pm -I Util -I Unix -I Unix/Intel -I Interfaces/LinkerInterface -IL ArgEnv -IL StdLib -IL Directory Cpm -o cpm/cpm
cd ..
