cp -R --preserve=timestamps svn/clean-tools/htoclean .

( cd git/clean-libraries; git checkout master Libraries/ArgEnvUnix)
mkdir -p clean-libraries
mv git/clean-libraries/* clean-libraries

( cd clean-libraries/Libraries/ArgEnvUnix ; make -f Makefile_no_pic ArgEnvC.o )

cd htoclean/htoclean\ source\ code
clm -I unix -I ../../clean-libraries/Libraries/ArgEnvUnix -h 4m -nt -nr htoclean -o htoclean
cd ../..

