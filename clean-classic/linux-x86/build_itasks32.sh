set -e
if test ! -d iTasks-SDK ; then
  git clone --depth 1 https://gitlab.science.ru.nl/clean-and-itasks/iTasks-SDK.git
fi

cd iTasks-SDK
sh init_deps.sh
cd ..

find . -iname ".git" | xargs rm -rf

cd iTasks-SDK/Dependencies/graph_copy/linux32
make
cd ../../../..
