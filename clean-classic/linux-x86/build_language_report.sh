set -e
# ./git_clone.sh clean-compiler-and-rts/language-report.git language-report
(cd git/language-report; git checkout master write_clean_manual)
mv git/language-report/write_clean_manual .

cd write_clean_manual
./download_nimbus_font_curl.sh
./download_liberation_font.sh
chmod +r *.ttf
clm -h 20m -nt -nr write_clean_manual -o write_clean_manual
./write_clean_manual
cd ..
