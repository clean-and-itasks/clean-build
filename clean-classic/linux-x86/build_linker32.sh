cp -R --preserve=timestamps svn/clean-tools/elf_linker .

cd elf_linker
clm -I . -I ia32 -IL ArgEnv -I ../compiler/main/Unix \
	-l ../compiler/main/Unix/set_return_code_c.o \
	-s 8m -h 64m -nt -nr linker -o linker
cd ..

