# ./git_clone.sh clean-compiler-and-rts/run-time-system.git RuntimeSystem
(cd git/run-time-system; git checkout master .)
mkdir run-time-system
mv git/run-time-system/* run-time-system

cd run-time-system
chmod +x ./remove_tmp_files_linux
./remove_tmp_files_linux
make -f Makefile.linux32_no_pic
./remove_tmp_files_linux
make -f Makefileprofile.linux32_no_pic
./remove_tmp_files_linux
make -f Makefiletrace.linux32_no_pic
cd ..
