# ./git_clone.sh clean-compiler-and-rts/stdenv.git stdenv
(cd git/stdenv; git checkout master .)
mkdir stdenv
mv git/stdenv/* stdenv/

mkdir -p clean/StdEnv
cp stdenv/_library.dcl clean/StdEnv
cp stdenv/_startup.dcl clean/StdEnv
cp stdenv/_startup.dcl clean/StdEnv/_startupTrace.dcl
cp stdenv/_startup.dcl clean/StdEnv/_startupProfile.dcl
cp stdenv/_system.dcl clean/StdEnv
for a in StdArray StdBool StdChar StdCharList StdClass StdDebug StdEnum StdEnv \
	 StdFunc StdFunctions StdList StdMisc StdOrdList StdOverloaded StdOverloadedList \
	 StdStrictLists StdTuple _SystemArray _SystemEnum _SystemEnumStrict \
	 _SystemStrictLists StdGeneric StdMaybe _SystemStrictMaybes _SystemDynamic StdDynamic;
do cp stdenv/$a.[di]cl clean/StdEnv ;
done

cp stdenv/StdInt.icl clean/StdEnv
cp stdenv/StdInt.dcl clean/StdEnv
cp stdenv/StdFile.icl clean/StdEnv
cp stdenv/StdFile.dcl clean/StdEnv
cp stdenv/StdReal.icl clean/StdEnv
cp stdenv/StdReal.dcl clean/StdEnv
cp stdenv/StdString.icl clean/StdEnv
cp stdenv/StdString.dcl clean/StdEnv

cp stdenv/Makefile.linux clean/StdEnv/Makefile
cp stdenv/make.sh clean/StdEnv/make.sh
cp stdenv/install.sh clean/StdEnv/install.sh

mkdir -p clean/StdEnv/Clean\ System\ Files
cp stdenv/Clean\ System\ Files/_system.abc clean/StdEnv/Clean\ System\ Files
cp run-time-system/_startup.o clean/StdEnv/Clean\ System\ Files
cp run-time-system/_startupTrace.o clean/StdEnv/Clean\ System\ Files
cp run-time-system/_startupProfile.o clean/StdEnv/Clean\ System\ Files

clean/exe/cg clean/StdEnv/Clean\ System\ Files/_system

echo first compile of system modules
for a in StdChar; # compile twice for inlining
do clean/exe/cocl -pt -pm -ou -P clean/StdEnv $a ;
done

echo second compile of system modules
for a in StdMisc StdBool StdInt StdChar StdFile StdReal StdString;
do clean/exe/cocl -pt -pm -ou -P clean/StdEnv $a ;
done

