mkdir git
cd git
../clean-classic/linux-x86/git_clone_no_checkout_depth_1.sh clean-compiler-and-rts/code-generator.git code-generator
../clean-classic/linux-x86/git_clone_no_checkout_depth_1.sh clean-compiler-and-rts/run-time-system.git run-time-system
# ../clean-classic/linux-x86/git_clone_no_checkout_depth_1.sh clean-compiler-and-rts/compiler.git compiler
../clean-classic/linux-x86/git_clone_no_checkout_no_single_branch_depth_1.sh clean-compiler-and-rts/compiler.git compiler
../clean-classic/linux-x86/git_clone_no_checkout_depth_1.sh clean-compiler-and-rts/language-report.git language-report
../clean-classic/linux-x86/git_clone_no_checkout_depth_1.sh clean-compiler-and-rts/stdenv.git stdenv
../clean-classic/linux-x86/git_clone_no_checkout_depth_1.sh clean-and-itasks/clm.git clm
../clean-classic/linux-x86/git_clone_no_checkout_depth_1.sh clean-and-itasks/clean-ide.git clean-ide
../clean-classic/linux-x86/git_clone_no_checkout_depth_1.sh clean-and-itasks/clean-libraries.git clean-libraries
../clean-classic/linux-x86/gitlab_clone_no_checkout_depth_1.sh clean-and-itasks/gast.git gast
../clean-classic/linux-x86/gitlab_clone_no_checkout_depth_1.sh clean-and-itasks/clean-platform.git clean-platform
cd ..
mkdir svn
cd svn
svn export https://svn.cs.ru.nl/repos/clean-tools/trunk clean-tools
svn export https://svn.cs.ru.nl/repos/clean-dynamic-system/trunk/dynamics/Examples/Examples\ No\ Linker DynamicsExamples
cd ..

