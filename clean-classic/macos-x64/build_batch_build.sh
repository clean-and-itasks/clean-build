set -e
# ./git_clone.sh clean-and-itasks/clean-ide.git clean-ide
(cd git/clean-ide; git checkout master .)
mkdir clean-ide
mv git/clean-ide/* clean-ide/

cd clean-ide/BatchBuild
sh make.macosx.sh
#clm -nt -nr -h 20m -s 2m -I BatchBuild -I Pm -I MacOSX -I Util -IL ArgEnv -IL StdLib -IL Directory BatchBuild -o batch_build
cd ..
clm -nt -nr -h 20m -I cpm -I cpm/Posix -I BatchBuild -I Pm -I Util -I MacOSX -I Interfaces/LinkerInterface -IL ArgEnv -IL StdLib -IL Directory Cpm -o cpm/cpm
cd ..
