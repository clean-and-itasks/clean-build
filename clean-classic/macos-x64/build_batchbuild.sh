set -e
if test ! -d tools/CleanIDE ; then
  ./svn_checkout.sh clean-ide/trunk tools/CleanIDE
fi

cd tools/CleanIDE/BatchBuild
sh make.macosx.sh
#clm -IL ArgEnv -IL StdLib -IL Directory -I BatchBuild -I Pm -I MacOSX -I Util -s 2m -h 20m BatchBuild -o batch_build
