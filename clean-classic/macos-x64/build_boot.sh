mkdir -p boot
cp -R -p clean boot/clean
cd boot/clean
make
cd ../..
mv boot/clean boot/clean0
cp -r -p clean boot/clean
cp txt/README.boot boot/clean/README
./remove_bin.sh
for m in StdArray StdEnv StdOverloaded _SystemArray StdOverloadedList \
	_SystemEnum StdFunc _SystemEnumStrict StdCharList StdStrictLists \
	_SystemStrictLists StdClass StdList StdDebug StdTuple StdEnum \
	StdOrdList StdGeneric;
do cp -p "boot/clean0/lib/StdEnv/Clean System Files/"$m.abc "boot/clean/StdEnv/Clean System Files/"$m.abc;
done

mkdir -p boot/clean/src
cp txt/Makefile_boot boot/clean/src/Makefile
cd boot/clean/src
#../../../export.sh RuntimeSystem
../../../svn_export.sh clean-run-time-system/trunk RuntimeSystem
#../../../export.sh CodeGenerator
../../../svn_export.sh clean-code-generator/trunk CodeGenerator
#../../../export.sh tools/clm
../../../svn_export.sh clean-tools/trunk/clm tools/clm
#../../../export.sh libraries/ArgEnvUnix
../../../svn_export.sh clean-libraries/trunk/Libraries/ArgEnvUnix libraries/ArgEnvUnix
#../../../export.sh compiler
../../../svn_export.sh clean-compiler/trunk compiler
#../../../export.sh tools/htoclean
../../../svn_export.sh clean-tools/trunk/htoclean tools/htoclean
rm CodeGenerator/CodeGenLib
rm CodeGenerator/CodeGenLib_o
rm CodeGenerator/CodeGenLib_xo
chmod +x RuntimeSystem/remove_tmp_files_linux
cd ../../..
for p in compiler/main/Unix libraries/ArgEnvUnix compiler/frontend compiler/backend compiler/main;
do  mkdir -p "boot/clean/src/$p/Clean System Files";
    cp "$p/Clean System Files/"*.abc "boot/clean/src/$p/Clean System Files";
done
