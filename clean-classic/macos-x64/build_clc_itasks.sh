set -e
# ./git_clone.sh clean-compiler-and-rts/compiler.git compiler
(cd git/compiler; git checkout origin/itask -- .)
mkdir compiler_itask
mv git/compiler/* compiler_itask/

cd compiler_itask
clm -ABC -nw -ci -I backend -I frontend -I main -I main/Unix -IL ArgEnv backendconvert
unix/make.macosx64.sh
cd ..
