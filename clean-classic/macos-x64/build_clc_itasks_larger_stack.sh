set -e
# ./git_clone.sh clean-compiler-and-rts/compiler.git compiler
(cd git/compiler; git checkout origin/itask -- .)
mkdir compiler_itask
mv git/compiler/* compiler_itask/

cp clm/clm boot_compiler/bin/clm_
(cd boot_compiler/bin; ./patch_bin clm_ `./patch_bin clm CLEANPATH | tr -d '='` )
(cd boot_compiler/bin; ./patch_bin clm_ `./patch_bin clm CLEANLIB | tr -d '='` )
(cd boot_compiler/bin; ./patch_bin clm_ `./patch_bin clm CLEANILIB | tr -d '='` )

cd compiler_itask
clm_ -aC,-s,2m -fusion -ABC -nw -ci -I backend -I frontend -I main -I main/Unix -IL ArgEnv backendconvert
unix/make.macosx64.sh
cd ..
