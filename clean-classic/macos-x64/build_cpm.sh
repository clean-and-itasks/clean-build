set -e
if test ! -d tools/CleanIDE ; then
  ./svn_checkout.sh clean-ide/trunk tools/CleanIDE
fi

cd tools/CleanIDE
batch_build CpmMacOSX.prj
