set -e
if test ! -d sapldynamics ; then
  ./svn_export.sh clean-dynamic-system/branches/itask sapldynamics
fi

cd sapldynamics
batch_build SaplCollectorLinkerMacOSX.prj
cd ..
