#!/bin/sh
set -e
mkdir -p target/clean-eastwood

# Build tools
export CLEAN_HOME=`pwd`/build/clean
export PATH=$CLEAN_HOME/bin:$PATH

cp -r src/eastwood-main build/clean-eastwood
mkdir -p target/clean-eastwood/bin

(cd build/clean-eastwood/src/linter/
  mv EastwoodLint.prj.default EastwoodLint.prj
  cpm project EastwoodLint.prj build
)

(cd build/clean-eastwood/src/languageServer/
  mv EastwoodCleanLanguageServer.prj.default EastwoodCleanLanguageServer.prj
  cpm project EastwoodCleanLanguageServer.prj build
)

cp build/clean-eastwood/src/linter/eastwood-lint target/clean-eastwood/bin/eastwood-lint
cp build/clean-eastwood/src/languageServer/eastwood-cls target/clean-eastwood/bin/eastwood-cls
