#!/bin/sh
set -e
mkdir -p target/clean-eastwood

# Build tools
export CLEAN_HOME=`cygpath --windows --absolute build/clean`
export PATH=$CLEAN_HOME:$CLEAN_HOME\\Tools:$PATH

cp -r src/eastwood-main build/clean-eastwood
mkdir -p target/clean-eastwood/bin

(cd build/clean-eastwood/src/linter/
  mv EastwoodLintWin.prj.default EastwoodLint.prj
  cpm project EastwoodLint.prj build
)

cp build/clean-eastwood/src/linter/eastwood-lint.exe target/clean-eastwood/bin/eastwood-lint.exe
