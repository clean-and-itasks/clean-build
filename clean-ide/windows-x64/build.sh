#!/bin/sh

#Visual studio compiler 
VSROOT="C:\Program Files (x86)\Microsoft Visual Studio 14.0"
#VSDEVENV=`cygpath --unix "$VSROOT\Common7\IDE\devenv.exe"`
#VSVARS="$VSROOT\VC\bin\vcvars32.bat"
#VSCC="$VSROOT\VC\bin\cl.exe"
VSSETENV="C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"

COMMAND=`cygpath --unix $COMSPEC`

mkdir -p build/CleanIDE
cp -r src/clean-ide-master/* build/CleanIDE

#Compile resource
mkdir -p build/idersrc
cp -r src/clean-ide-master/WinSupport/* build/idersrc/
(cd "build/idersrc"
	cat <<EOBATCH | "$COMMAND"
@		call "$VSSETENV" amd64
@		call makeresource64.bat
EOBATCH
)
cp "build/idersrc/winIde.rsrc"  "build/CleanIDE/WinSupport/winIde.rsrc"


export CLEAN_HOME=`cygpath --windows --absolute build/clean`
export PATH=$CLEAN_HOME\\Tools:$PATH

(cd "build/CleanIDE"
     ../clean/cpm.exe WinIde64.prj
     ../clean/cpm.exe WinTimeProfile64.prj
     ../clean/cpm.exe WinHeapProfile64.prj
)
TARGET=target/clean-ide

mkdir -p $TARGET
cp build/CleanIDE/CleanIDE.exe $TARGET/
mkdir -p $TARGET/Tools/Time\ Profiler
cp build/CleanIDE/ShowTimeProfile.exe $TARGET/Tools/Time\ Profiler/
mkdir -p $TARGET/Tools/Heap\ Profiler
cp build/CleanIDE/ShowHeapProfile.exe $TARGET/Tools/Heap\ Profiler/

#Add default config
mkdir -p $TARGET/Config
cp src/clean-ide-master/Config/windows.km $TARGET/Config/default.km
cp src/clean-ide-master/Config/IDEPrefs $TARGET/Config/IDEPrefs
