#!/bin/sh
set -e

export CLEAN_HOME=`pwd`/build/clean
export PATH=$CLEAN_HOME/bin:$PATH


mkdir -p build
cp -r src/language-report-master/write_clean_manual build/clean-language-report

(cd build/clean-language-report
	#Create generator
	clm -h 20M write_clean_manual -o write_clean_manual

	#Copy fonts
	cp ../../Fonts/LiberationSans-Regular.ttf .
	cp ../../Fonts/LiberationSans-Bold.ttf .
	cp ../../Fonts/LiberationSans-Italic.ttf .
	cp ../../Fonts/NimbusMonoPS-Regular.ttf .
	cp ../../Fonts/NimbusMonoPS-Bold.ttf .

	#Generate manual
	./write_clean_manual
)
#Copy to target
mkdir -p target/clean-language-report/doc
cp build/clean-language-report/CleanLanguageReport.* target/clean-language-report/doc/
