#!/bin/sh
set -e

export CLEAN_HOME=`cygpath --windows --absolute build/clean`
export PATH=$CLEAN_HOME:$CLEAN_HOME\\Tools:$PATH

mkdir -p build
cp -r src/language-report-master/write_clean_manual build/clean-language-report

(cd build/clean-language-report
	#Create generator
	cpm.exe project write_clean_manual create
	cpm.exe project write_clean_manual.prj set -h 20M
	cpm.exe write_clean_manual.prj

	#Copy fonts
	cp ../../Fonts/LiberationSans-Regular.ttf .
	cp ../../Fonts/LiberationSans-Bold.ttf .
	cp ../../Fonts/LiberationSans-Italic.ttf .
	cp ../../Fonts/NimbusMonoPS-Regular.ttf .
	cp ../../Fonts/NimbusMonoPS-Bold.ttf .

	#Generate manual
	./write_clean_manual.exe
)
#Copy to target
mkdir -p target/clean-language-report/Help
cp build/clean-language-report/CleanLanguageReport.* target/clean-language-report/Help/
