#!/bin/sh
TARGET=target/clean-lib-argenv

mkdir -p build
cp -r src/clean-libraries-master/Libraries/ArgEnvWindows build/ArgEnv
mkdir -p "$TARGET/Libraries/ArgEnv/Clean System Files"
for f in ArgEnvC.c ArgEnv.dcl printenv.icl ArgEnv.icl README.txt;
do  cp build/ArgEnv/$f $TARGET/Libraries/ArgEnv/$f
done
cp build/ArgEnv/Clean\ System\ Files/* "$TARGET/Libraries/ArgEnv/Clean System Files/"
