#!/bin/bash
set -e
mkdir -p target/clean-lib-compiler

# Add libraries
mkdir -p target/clean-lib-compiler/lib
cp -r src/compiler-master target/clean-lib-compiler/lib/compiler
cp -r src/compiler-itask target/clean-lib-compiler/lib/compiler-itask

function build_backend() {
	make -C $1/main/Unix
	make -C $1/backendC/CleanCompilerSources -f Makefile.linux64
	mkdir -p $1/backend/Clean\ System\ Files
	ln -fs $1/backendC/CleanCompilerSources/backend.a $1/backend/Clean\ System\ Files/backend_library
}

build_backend target/clean-lib-compiler/lib/compiler
build_backend target/clean-lib-compiler/lib/compiler-itask
