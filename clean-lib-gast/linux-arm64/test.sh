set -e
# Set up
mkdir -p "test/clean"
cp -r build/clean/* test/clean/
cp -r src/gast-master/Libraries/ test/Libraries/
cp -r src/gast-master/Tests/ test/Tests/

# Compile and run
(cd "test"
	export CLEAN_HOME=`pwd`/clean
	export PATH=$CLEAN_HOME/bin:$PATH
	echo $CLEAN_HOME

	make -C Tests test
)
