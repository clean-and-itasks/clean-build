#!/bin/sh
PACKAGE=$1
OS=$2
ARCH=$3

mkdir -p target/clean-lib-gast

# Add libraries
mkdir -p target/clean-lib-gast/Libraries/Gast
cp -r src/gast-master/Libraries/* target/clean-lib-gast/Libraries/Gast/

# Add environments
mkdir -p target/clean-lib-gast/Config
cp src/gast-master/Config/windows-x64/Gast.env target/clean-lib-gast/Config/Gast.env
