#!/bin/sh
. ./config.sh # Path to cross compilers

mkdir -p target/clean-lib-graphcopy

# Add clean libraries
mkdir -p target/clean-lib-graphcopy/lib/GraphCopy
cp -r src/clean-graph-copy/common/* target/clean-lib-graphcopy/lib/GraphCopy/
cp -r src/clean-graph-copy/linux/*.[id]cl target/clean-lib-graphcopy/lib/GraphCopy/

# Build c library
mkdir -p build/clean-graph-copy
cp -r src/clean-graph-copy/common build/clean-graph-copy/
cp -r src/clean-graph-copy/linux build/clean-graph-copy/
cp -r src/clean-graph-copy/arm32 build/clean-graph-copy/
cp -r src/clean-graph-copy/tools build/clean-graph-copy/
(cd build/clean-graph-copy/arm32
    make -e CC=$CROSS_CC CFLAGS="$CROSS_CFLAGS" AS="$CROSS_AS" ASFLAGS="$CROSS_ASFLAGS"
)
cp -r build/clean-graph-copy/arm32/Clean\ System\ Files target/clean-lib-graphcopy/lib/GraphCopy/
