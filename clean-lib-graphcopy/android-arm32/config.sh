#!/bin/sh
export CC=/usr/bin/gcc
export AS=as
export LD=/usr/bin/ld
export CFLAGS=''

# Android
export NDK=$(pwd)/dependencies/android-ndk/android-ndk-r13b
export NDK_TOOLCHAIN=${NDK}/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin
export CROSS_COMPILE=arm-linux-androideabi
export SYSROOT=${NDK}/platforms/android-21/arch-arm/

export CROSS_CC=${NDK_TOOLCHAIN}/${CROSS_COMPILE}-gcc
export CROSS_AS=${NDK_TOOLCHAIN}/${CROSS_COMPILE}-as
export CROSS_LD=${NDK_TOOLCHAIN}/${CROSS_COMPILE}-ld
export CROSS_CFLAGS="--sysroot=${SYSROOT} -pie -march=armv7-a -msoft-float -DPIC -DANDROID"
export CROSS_ASFLAGS="--defsym PIC=1"
export CROSS_LDFLAGS=""
