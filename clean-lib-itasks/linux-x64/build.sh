#!/bin/sh
set -e
mkdir -p target/clean-lib-itasks

# Update node modules (continue when installation of node fails; we cannot install on the Jenkins server)
(curl -sL https://deb.nodesource.com/setup_16.x | bash -; apt-get install -qq -y nodejs) || echo "no permissions to install node"
(cd src/itasks-sdk-master/Libraries; npm ci)

# Add libraries
mkdir -p target/clean-lib-itasks/lib
cp -r src/itasks-sdk-master/Libraries target/clean-lib-itasks/lib/iTasks

# Add examples
mkdir -p target/clean-lib-itasks/examples/iTasks
cp -r src/itasks-sdk-master/Examples/* target/clean-lib-itasks/examples/iTasks/
find target/clean-lib-itasks -name "*.prj.default" | while read f; do
		mv "$f" "$(dirname $f)/$(basename -s .prj.default $f)".prj
	done

# Build tools
export CLEAN_HOME=`pwd`/build/clean
export PATH=$CLEAN_HOME/bin:$PATH

# Web collector
mkdir -p build/itasks-web-collector
mkdir -p target/clean-lib-itasks/lib/exe
cp -r src/itasks-sdk-master/Tools/WebResourceCollector.icl build/itasks-web-collector/
cp -r src/itasks-sdk-master/Tools/WebResourceCollector.prj.default build/itasks-web-collector/WebResourceCollector.prj
cp -r src/itasks-sdk-master/Libraries/* build/itasks-web-collector # the web collector requires modules from the iTasks library
(cd build/itasks-web-collector
    cpm WebResourceCollector.prj
)
cp build/itasks-web-collector/WebResourceCollector target/clean-lib-itasks/lib/exe/itasks-web-collector

# Create basicapiexamples aggregated module
mkdir -p build/basicapiexamples
cp -r src/itasks-sdk-master/Examples/* build/basicapiexamples/
(cd build/basicapiexamples
    cat CreateBasicAPIExamples.prj.default > CreateBasicAPIExamples.prj
    cpm CreateBasicAPIExamples.prj
    ./CreateBasicAPIExamples.exe > BasicAPIExamples.icl
)
cp build/basicapiexamples/BasicAPIExamples.icl target/clean-lib-itasks/examples/iTasks/
#Remove create program
rm target/clean-lib-itasks/examples/iTasks/CreateBasicAPIExamples.icl
rm target/clean-lib-itasks/examples/iTasks/CreateBasicAPIExamples.prj

# Add environments
mkdir -p target/clean-lib-itasks/etc
cp src/itasks-sdk-master/Config/iTasks.prt target/clean-lib-itasks/etc/iTasks.prt
cp src/itasks-sdk-master/Config/linux-x64/iTasks.env target/clean-lib-itasks/etc/iTasks.env
