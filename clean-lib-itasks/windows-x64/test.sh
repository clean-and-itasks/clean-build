set -e
# Set up
mkdir -p "test/clean"
mkdir -p "test/itasks-sdk"
cp -r build/clean/* test/clean/
cp -r target/clean-lib-itasks/* test/clean/
cp -r src/itasks-sdk-master/* test/itasks-sdk/
tail -n +3 test/clean/Config/iTasks.env >> test/clean/Config/IDEEnvs

# Compile and run
export CLEAN_HOME=`cygpath --windows --absolute test/clean`
export PATH=$CLEAN_HOME\\Tools:$CLEAN_HOME:$PATH
bash test/itasks-sdk/Tests/windows.sh
