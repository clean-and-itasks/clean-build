#!/bin/bash

PACKAGE=$1
OS=$2
ARCH=$3

#Visual studio compiler (only included for building temporary ObjectIO)
# MS Visual .NET
if test -e "C:\Program Files\Microsoft Visual Studio 14.0"; then
	VSROOT="C:\Program Files\Microsoft Visual Studio 14.0"
	VSDEVENV=`cygpath --unix "$VSROOT\Common7\IDE\devenv.exe"`
	VSVARS="$VSROOT\VC\bin\vcvars32.bat"
	VSCC="$VSROOT\VC\bin\cl.exe"
else
	if test -e "C:\Program Files\Microsoft Visual Studio .NET 2003"; then
		VSROOT="C:\Program Files\Microsoft Visual Studio .NET 2003"
		VSDEVENV=`cygpath --unix "$VSROOT\Common7\IDE\devenv.exe"`
		VSVARS="$VSROOT\Common7\Tools\vsvars32.bat"
		VSCC="$VSROOT\VC7\BIN\cl.exe"
	else
		VSROOT="C:\Program Files\Microsoft Visual Studio"
		VSDEVENV=`cygpath --unix "$VSROOT\Common\MSDev98\Bin\msdev.exe"`
		VSVARS="$VSROOT\VC98\Bin\vcvars32.bat"
		VSCC="$VSROOT\VC98\Bin\cl.exe"
	fi
fi


#VSROOT="C:\Program Files\Microsoft Visual Studio 14.0"
#VSDEVENV=`cygpath --unix "$VSROOT\Common7\IDE\devenv.exe"`
#VSVARS="$VSROOT\VC\bin\vcvars32.bat"
#VSCC="$VSROOT\VC\bin\cl.exe"

COMMAND=`cygpath --unix $COMSPEC`

vscc() { # $1: C .c file path
	vscc_file=`cygpath --absolute --windows "$1"`
	vscc_dir=`dirname "$1"`
	vscc_object_file=`basename $vscc_file .c`.o
	(cd "$vscc_dir"
	cat <<EOBATCH | "$COMMAND"
@"$VSVARS"
@cl /nologo /c /O /GS- "$vscc_file" /Fo"$vscc_object_file"
EOBATCH
	)
}

TARGET=target/clean-lib-objectio

mkdir -p build/objectio
cp -r src/clean-libraries-master/Libraries/ObjectIO/ObjectIO/OS\ Windows/Windows_C_12/* build/objectio/

#Copy everything !ONLY BECAUSE THIS IS THE ONLY WAY TO BUILD THE CleanIDE...
mkdir -p "$TARGET/Libraries/ObjectIO"
cp -r src/clean-libraries-master/Libraries/ObjectIO/ObjectIO/* $TARGET/Libraries/ObjectIO/

#Compile c files
#for c in build/objectio/*.c; do
#   echo $c
#   vscc "$c"
#done

#Copy .o files to target
#mkdir -p "$TARGET/Libraries/ObjectIO/OS Windows/Clean System Files"
#cp build/objectio/*.o "$TARGET/Libraries/ObjectIO/OS Windows/Clean System Files/"

# Copy clean files to target
#cp src/clean-libraries/Libraries/ObjectIO/ObjectIO/*.[id]cl $TARGET/Libraries/ObjectIO/
#cp src/clean-libraries/Libraries/ObjectIO/ObjectIO/OS\ Windows/*.[id]cl $TARGET/Libraries/ObjectIO/OS\ Windows/
#cp src/clean-libraries/Libraries/ObjectIO/ObjectIO/OS\ Windows/Clean\ System\ Files/*_library $TARGET/Libraries/ObjectIO/OS\ Windows/Clean\ System\ Files/

# Copy examples
mkdir -p $TARGET/Examples/ObjectIO
cp -r src/clean-libraries-master/Examples/ObjectIO/ObjectIO\ Examples/* $TARGET/Examples/ObjectIO/
sed -i 's/ObjectIO Examples/ObjectIO/g' $TARGET/Examples/ObjectIO/*/*.prj

# Copy environment
mkdir -p $TARGET/Config
cp $PACKAGE/$OS-$ARCH/txt/ObjectIO.env $TARGET/Config/
