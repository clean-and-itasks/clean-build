set -e
# Set up
mkdir -p "test/clean"
cp -r build/clean/* test/clean/
cp -r target/clean-lib-objectio/* test/clean/
tail -n +2 test/clean/Config/ObjectIO.env >> test/clean/Config/IDEEnvs

# Compile and run
export CLEAN_HOME=`cygpath --windows --absolute test/clean`
export PATH=$CLEAN_HOME\\Tools:$PATH
(cd "test/clean/Examples/ObjectIO"
   # Compile examples (to check all examples still build)
   (cd bounce
     ../../../cpm.exe bounce.prj
   )
)
