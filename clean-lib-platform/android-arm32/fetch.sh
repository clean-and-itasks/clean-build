#!/bin/sh
GIT=git
GIT_BASEURL="https://gitlab.com/clean-and-itasks"

mkdir -p src
rm -rf src/clean-platform
$GIT clone $GIT_BASEURL/clean-platform "src/clean-platform"
if [ -n "${CLEANDATE+set}" ]; then
	cd "src/clean-platform"
	$GIT checkout `$GIT rev-list -n 1 --before="$CLEANDATE" master`
fi
