#!/bin/sh
mkdir -p dependencies

# Android NDK
if [ ! -f "dependencies/ndk-r13b.zip" ]; then
        curl -L -o dependencies/ndk-r13b.zip https://dl.google.com/android/repository/android-ndk-r13b-linux-x86_64.zip
fi
(cd dependencies
        mkdir -p android-ndk
        unzip ndk-r13b.zip -d ./android-ndk
)
