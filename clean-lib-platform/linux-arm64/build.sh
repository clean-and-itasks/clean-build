#!/bin/sh
mkdir -p target/clean-lib-platform

# Add libraries
mkdir -p target/clean-lib-platform/lib/Platform

# Compile c dependencies
make -C src/clean-platform-master/src/cdeps install

cp -r src/clean-platform-master/src/libraries/OS-Independent/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform-master/src/libraries/OS-Posix/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform-master/src/libraries/OS-Linux/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform-master/src/libraries/OS-Linux-64/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform-master/src/libraries/Platform-ARM/* target/clean-lib-platform/lib/Platform/
