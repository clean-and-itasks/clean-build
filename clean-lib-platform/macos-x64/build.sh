#!/bin/sh
mkdir -p target/clean-lib-platform

# Add libraries
mkdir -p target/clean-lib-platform/lib/Platform

# Compile c dependencies
make -C src/clean-platform-master/src/cdeps install
make -C src/clean-platform-master/src/cdeps/OS-Posix install

cp -r src/clean-platform-master/src/libraries/OS-Independent/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform-master/src/libraries/OS-Posix/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform-master/src/libraries/OS-Mac/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform-master/src/libraries/Platform-x86/* target/clean-lib-platform/lib/Platform/
