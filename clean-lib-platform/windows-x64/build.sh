#!/bin/sh
set -e

#Visual studio compiler
VSSETENV="C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"
COMMAND=`cygpath --unix $COMSPEC`

mkdir -p target/clean-lib-platform

# Add libraries
mkdir -p target/clean-lib-platform/Libraries/Platform

# Compile c dependencies
make -C src/clean-platform-master/src/cdeps install
# Build
(cd src/clean-platform-master/src/cdeps/OS-Windows
	cat <<EOBATCH | "$COMMAND"
@call "$VSSETENV" amd64
@nmake -f Makefile install
EOBATCH
)

cp -r src/clean-platform-master/src/libraries/OS-Independent/* target/clean-lib-platform/Libraries/Platform/
cp -r src/clean-platform-master/src/libraries/OS-Windows/* target/clean-lib-platform/Libraries/Platform/
cp -r src/clean-platform-master/src/libraries/OS-Windows-64/* target/clean-lib-platform/Libraries/Platform/
cp -r src/clean-platform-master/src/libraries/Platform-x86/* target/clean-lib-platform/Libraries/Platform/
