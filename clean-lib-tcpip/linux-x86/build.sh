#!/bin/sh

TARGET_DIR=target/clean-lib-tcpip/lib/TCPIP

# Copy Clean modules
mkdir -p $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPIP.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPIP.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPDef.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPDef.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPEvent.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPEvent.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPChannelClass.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPChannelClass.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPChannels.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPChannels.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPStringChannels.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPStringChannels.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPStringChannelsInternal.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/TCPStringChannelsInternal.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/tcp_bytestreams.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/tcp_bytestreams.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/Linux_C/tcp.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/Linux_C/tcp.icl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/Linux_C/ostcp.dcl $TARGET_DIR
cp src/clean-libraries-master/Libraries/TCPIP/Linux_C/ostcp.icl $TARGET_DIR

# Build and copy C interface
mkdir -p build/TCPIP/Clean\ System\ Files
cp src/clean-libraries-master/Libraries/TCPIP/Linux_C/cTCP_121.c build/TCPIP/Clean\ System\ Files/
cp src/clean-libraries-master/Libraries/TCPIP/Linux_C/cTCP_121.h build/TCPIP/Clean\ System\ Files/
cp src/clean-tools-trunk/htoclean/Clean.h build/TCPIP/Clean\ System\ Files/
(cd build/TCPIP/Clean\ System\ Files
    gcc -m32 -c cTCP_121.c -o cTCP_121.o
)
mkdir -p $TARGET_DIR/Clean\ System\ Files/
cp build/TCPIP/Clean\ System\ Files/cTCP_121.o $TARGET_DIR/Clean\ System\ Files/
