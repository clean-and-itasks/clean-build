#!/bin/sh
set -e
mkdir -p target/clean-test-properties

# Build tools
export CLEAN_HOME=`pwd`/build/clean
export PATH=$CLEAN_HOME/bin:$PATH

cp -r src/clean-test-properties-master build/clean-test-properties
mkdir -p target/clean-test-properties/bin

make -C build/clean-test-properties/src

cp build/clean-test-properties/src/testproperties target/clean-test-properties/bin/testproperties
