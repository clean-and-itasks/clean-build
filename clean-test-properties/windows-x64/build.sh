#!/bin/sh
set -e
mkdir -p target/clean-test-properties

# Build tools
VSSETENV="C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"
COMMAND=`cygpath --unix $COMSPEC`

export CLEAN_HOME=`cygpath --windows --absolute build/clean`
export PATH=$CLEAN_HOME:$CLEAN_HOME\\Tools:$PATH

cp -r src/clean-test-properties-master/src build/clean-test-properties

(cd build/clean-test-properties/compiler/backendC/CleanCompilerSources
	cat <<EOBATCH | "$COMMAND"
@call "$VSSETENV" amd64
@nmake -f Makefile.windows64
EOBATCH
)

(cd build/clean-test-properties
	mv testproperties_win.prj.default testproperties.prj
	cpm.exe project testproperties.prj build
)

cp build/clean-test-properties/testproperties.exe target/clean-test-properties/testproperties.exe
