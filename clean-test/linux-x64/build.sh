#!/bin/sh
set -e
mkdir -p target/clean-test

# Build tools
export CLEAN_HOME=`pwd`/build/clean
export PATH=$CLEAN_HOME/bin:$PATH

cp -r src/clean-test-master build/clean-test
mkdir -p target/clean-test/bin

(cd build/clean-test
  mv cleantest.prj.default cleantest.prj
  cpm project cleantest.prj build
)

cp build/clean-test/cleantest target/clean-test/bin/cleantest
