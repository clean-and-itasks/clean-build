#!/bin/sh
set -e
mkdir -p target/clean-test

# Build tools
export CLEAN_HOME=`cygpath --windows --absolute build/clean`
export PATH=$CLEAN_HOME:$CLEAN_HOME\\Tools:$PATH

cp -r src/clean-test-master build/clean-test

(cd build/clean-test
  mv cleantest_win.prj.default cleantest.prj
  cpm.exe project cleantest.prj build
)

cp build/clean-test/cleantest.exe target/clean-test/cleantest.exe
